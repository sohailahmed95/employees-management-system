<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePunishmentRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('punishment_record', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nature_of_offence')->nullable();
            $table->string('punishment_awarded')->nullable();
            $table->string('punished_by')->nullable();
            $table->integer('employee_id')->unsigned();

            $table->softDeletes();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('punishment_record');
    }
}
