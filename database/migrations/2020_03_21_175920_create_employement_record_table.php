<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployementRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employement_record', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unit_id')->unsigned();

            $table->dateTime('from')->nullable();
            $table->dateTime('to')->nullable();

            $table->integer('employee_id')->unsigned();
            $table->softDeletes();

            $table->timestamps();


            $table->foreign('unit_id')
                 ->references('id')->on('unit')
                 ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employement_record');
    }
}
