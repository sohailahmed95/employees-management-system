<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Unit extends Model
{
	    use SoftDeletes;
	
    protected $table = 'unit';
      protected $fillable = [
        'id','name'
    ];

     public function employement_record()
    {
        return $this->belongsTo('App\EmpRec');
    }
      public function employee_record()
    {
        return $this->belongsTo('App\Employee');
    }
}
