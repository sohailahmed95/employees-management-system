<?php

namespace App\Http\Controllers\Admin;

use App\TechQual;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TechQualController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = TechQual::where('employee_id',$request->employee_id)->get();
         // dd($data);
        return view('employee.edit.techqual',compact('data'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data = TechQual::with('employee')->where('employee_id',$request->employee_id)->get();
        // $units = Unit::all();
                
        return view('employee.add.techqual',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, TechQual $techQual)
    {
      // dd($request->t_course_name[]);
        // $records = EmpRec::where('employee_id',$request->employee_id)->get();

        // foreach ($request->all() as $key => $value) {
     
        foreach ($request->t_course_name as $key => $value) {
        $data = [
            'employee_id' => $request->employee_id,
            'course' => $request->t_course_name[$key],
            'institute' => $request->t_institue[$key],
            'division' => $request->t_division[$key],
            'from' => $request->t_fromDate[$key],
            'to' => $request->t_toDate[$key],
           
          ];

        $techQual->create($data);
        }

        $request->session()->flash('alert-success', 'Record Added!');
   


        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TechQual  $techQual
     * @return \Illuminate\Http\Response
     */
    public function show(TechQual $techQual)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TechQual  $techQual
     * @return \Illuminate\Http\Response
     */
    public function edit(TechQual $techQual)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TechQual  $techQual
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TechQual $techQual)
    {
        $records = TechQual::where('employee_id',$request->employee_id)->get();
// dd($records);
        foreach ($records as $key => $value) {
        $updates = [
            'course' => $request->t_course_name[$key],
            'division' => $request->t_division[$key],
            'institute' => $request->t_institue[$key],
            'from' => $request->t_fromDate[$key],
            'to' => $request->t_toDate[$key],
          ];
        $techQual->where([
            'course' => $value->course,
            'division' => $value->division,
            'institute' => $value->institute,
            'from' => $value->from,
            'to' => $value->to,
            'employee_id'=>$request->employee_id])->update($updates);
        }
        $request->session()->flash('alert-success', 'Record Updated!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TechQual  $techQual
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        // dd($request->all());
        TechQual::where('from', $request->from)->where('to', $request->to)->delete();
           $message =  $request->session()->flash('alert-danger', 'Record Deletd!');
        
         return response()->json([
        'status' => 1, 
        'message' => $message
       ], 200 );
        // return redirect()->back();
    }
}
