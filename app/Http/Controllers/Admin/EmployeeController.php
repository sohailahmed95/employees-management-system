<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use App\Employee;
use App\CivQual;
use App\TechQual;
use App\PunishRec;
use App\EmpRec;
use App\Unit;
use DB;

class EmployeeController extends Controller
{

  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $units = Unit::all();
        // $units = Unit::all();
        return view('employee.create',compact('units'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // dd($request->all());
        // $current_date = date('Y-m-d H:i:s');
        // $data = $request->except('_token');

        //  $rules = [
        //    'employee_id' =>
        //     'image' =>
        //     'rank' =>
        //     'bps' => 
        //     'name' =>
        //     'date_of_birth' =>
        //     'cnic' => 
        //     'domicile' =>
        //     'blood_group' =>
        //     'email' =>
        //     'marital_status' =>
        //     'no_of_children' =>
        //     'present_address' =>
        //     'permanent_address' =>
        //     'date_of_enrolment' =>
        //     'present_unit_id' =>
        //     'date_of_present_rank' =>
        //     'utilization_of_posting' =>
        //     'completion_of_25yrs' =>
        //     'superannuation' =>
        //     'qual_at_time_enrol' =>
        //     'division' => 
        // ];
       
        // $errors = [
          
        // ];

        // $validator = Validator::make($request->all(), $rules, $errors);

        // if ($validator->fails()) {
        // return redirect()->back()->with('errors',$validator->errors())->withInput();               
        // }
         $file = $request->file('image');
         $image = null;
         if (file_exists($file)) {
              
        $file_loc = $file->move(public_path('images'), $file->getClientOriginalName());
        $image = $file_loc->getFileName();
          // $build = new Builds();
          // $build->app_type_id = $request->input('project_type');
          // $build->build_type_id = $request->input('build_type');
          // $build->project_id = $request->input('project_name');
          // $build->build_num =$request->input('build_num');
          // $build->app_version =$request->input('app_version');
          // $build->release_note =$request->input('release_note');
          // $build->build_file = $file_loc->getFileName();
          // $build->uploaded_by = auth()->user()->name;
          // $build->save();

                  
        }
        $emp_info = [
            'employee_id' => $request->employee_id,
            'image' => $image,
            'rank' => $request->rank,
            'bps' => $request->bps,
            'name' => $request->name,
            'date_of_birth' => $request->date_of_birth,
            'cnic' => $request->cnic,
            'domicile' => $request->domicile,
            'blood_group' => $request->blood_group,
            'email' => $request->email,
            'marital_status' => $request->marital_status,
            'no_of_children' => $request->no_of_children,
            'present_address' => $request->present_address,
            'permanent_address' => $request->permanent_address,
            'date_of_enrolment' => $request->date_of_enrolment,
            'present_unit_id' => $request->present_unit_id,
            'date_of_present_rank' => $request->date_of_present_rank,
            'utilization_of_posting' => $request->utilization_of_posting,
            'completion_of_25yrs' => $request->completion_of_25yrs,
            'superannuation' => $request->superannuation,
            'qual_at_time_enrol' => $request->qual_at_time_enrol,
            'division' => $request->division
        ];
        Employee::create($emp_info);

        foreach ($request->c_course_name as $key => $value) {
        $civ_qual = [
            'employee_id' => $request->employee_id,
            'course' => $request->c_course_name[$key],
            'division' => $request->c_division[$key],
          ];

        CivQual::create($civ_qual);
        }  

        foreach ($request->t_course_name as $key => $value) {
        $tech_qual = [
            'employee_id' => $request->employee_id,
            'course' => $request->t_course_name[$key],
            'division' => $request->t_division[$key],
            'institute' => $request->t_institue[$key],
            'from' => $request->t_fromDate[$key],
            'to' => $request->t_toDate[$key],
          ];

        TechQual::create($tech_qual);
        }

        foreach ($request->punishment_name as $key => $value) {
        $punish_rec = [
            'employee_id' => $request->employee_id,
            'nature_of_offence' => $request->nature_offence[$key],
            'punishment_awarded' => $request->punishment_name[$key],
            'punished_by' => $request->punished_by[$key],
           
          ];

        PunishRec::create($punish_rec);
        }

            foreach ($request->emp_unit_id as $key => $value) {
        $emp_rec = [
            'employee_id' => $request->employee_id,
            'unit_id' => $request->emp_unit_id[$key],
            'from' => $request->emp_unitFromDate[$key],
            'to' => $request->emp_unitToDate[$key],
           
          ];

        EmpRec::create($emp_rec);
        }


        // dd($civ_qual);
        // die();
        // dd($emp_info);

        // $data['_token'] = null;
        // DB::table('employee_info')->insert(array_merge($request->except('_token'),['image'=>$request->image->GetClientOriginalName(),'created_at'=> $current_date,  'updated_at' =>$current_date]));
        
        // DB::table('civil_qualification')->insert([
        //     'course' =>$request->c_course_name[0],
        //     'division' => $request->c_division[0],
        //     'employee_id' => $request->emp_id
        // ]);
        // dd($data);
        $request->session()->flash('alert-success', 'New Employee Record Added!');
        
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        $data = $employee->all();

        return view('employee.search',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit( Request $request)
    {
      // var_dump($request->employee_id);
      // die();

        // $apps = AppProject::select('app_type_id')->where('project_id',$request->project)->get();

         $data = Employee::select('*')->where('employee_id',$request->employee_id)->first();
         if($data)
         {
            $units = Unit::all();

            return view('employee.edit.employee_info',compact('data','units'));
         }
         return view('');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
         $file = $request->file('image');
         // $image = null;
         if (file_exists($file)) {
              
        $file_loc = $file->move(public_path('images'), $file->getClientOriginalName());
        $image = $file_loc->getFileName();
      }
      else{
        $image = $request->file_update;
      }
        $update = [
            'employee_id' => $request->employee_id,
            'image' => $image,
            'rank' => $request->rank,
            'bps' => $request->bps,
            'name' => $request->name,
            'date_of_birth' => $request->date_of_birth,
            'cnic' => $request->cnic,
            'domicile' => $request->domicile,
            'blood_group' => $request->blood_group,
            'email' => $request->email,
            'marital_status' => $request->marital_status,
            'no_of_children' => $request->no_of_children,
            'present_address' => $request->present_address,
            'permanent_address' => $request->permanent_address,
            'date_of_enrolment' => $request->date_of_enrolment,
            'present_unit_id' => $request->present_unit_id,
            'date_of_present_rank' => $request->date_of_present_rank,
            'utilization_of_posting' => $request->utilization_of_posting,
            'completion_of_25yrs' => $request->completion_of_25yrs,
            'superannuation' => $request->superannuation,
            'qual_at_time_enrol' => $request->qual_at_time_enrol,
            'division' => $request->division
        ];

        $employee->where('employee_id', $request->employee_id)->update($update);
        $request->session()->flash('alert-success', 'Record Updated!');
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    } 


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function validate_empID(Request $request)
    {
        $rules = [
            'employee_id' => 'unique:employee_info,employee_id',          
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
        return response()->json(false);
               
             }
        return response()->json(true);


    }

       public function view(Request $request)
    {

      // $emp = EmpRec::orderBy('id','desc')->first();
      // dd($emp->unit);
      // $data  =   EmpRec::with('units')->where('employee_id',$request->employee_id)->first();
      $data  =   Employee::with('employement_record','punishment_record','civil_qualification','tech_qualification')->where('employee_id',$request->employee_id)->first();
      // dd($data);
      // $data2  =   Employee::with('employement_record','punishment_record','civil_qualification','tech_qualification')->where('employee_id',$request->employee_id)->get();
      // dd($data);
        return view('employee.view',compact('data'));
     
    }

      public function search_filter(Employee $employee, Request $request)
    {
       dd($request->all());
    }
}
