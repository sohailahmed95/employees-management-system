<?php

namespace App\Http\Controllers\Admin;

use App\Unit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use DB;

class UnitController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $units = Unit::all();
       
        return view('admin.unit',compact('units'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // dd($request->all());
        $rules = [
            'unit' => 'required'
        ];
        $errors = [
            'unit.required' => 'Please Enter Unit Name!'
            ];

        $validator = Validator::make($request->all(), $rules, $errors);
                
        if ($validator->fails()) {
            // dd('fail');
        return redirect()->back()->with('errors',$validator->errors());          
        }

        $unit = new Unit;
        $unit->name = $request->unit;
        $unit->save();
       // DB::table('unit')->insert($request->unit);
            // dd('pass');
        $request->session()->flash('alert-success', 'New Unit Added!');
        return redirect()->back();          

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $unit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        // $unit = Unit::all();
        // dd($id);
        $unit = Unit::find($id);

        $unit->delete();
        $request->session()->flash('alert-danger', 'Unit Deleted!');
        return redirect()->back();  
    }
}
