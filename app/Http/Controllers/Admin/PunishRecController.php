<?php

namespace App\Http\Controllers\Admin;

use App\PunishRec;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PunishRecController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = PunishRec::where('employee_id',$request->employee_id)->get();
         // dd($data);
        return view('employee.edit.punish_rec',compact('data'));  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
         $data = PunishRec::with('employee')->where('employee_id',$request->employee_id)->get();
        // $units = Unit::all();
                
        return view('employee.add.punish_rec',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, PunishRec $punishRec)
    {

        // dd($request->all());
         // $records = EmpRec::where('employee_id',$request->employee_id)->get();

        // foreach ($request->all() as $key => $value) {
     
        foreach ($request->punishment_name as $key => $value) {
        $data = [
            'employee_id' => $request->employee_id,
            'punishment_awarded' => $request->punishment_name[$key],
            'nature_of_offence' => $request->nature_offence[$key],
            'punished_by' => $request->punished_by[$key],
           
          ];

        $punishRec->create($data);
        }

        $request->session()->flash('alert-success', 'Record Added!');
   


        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PunishRec  $punishRec
     * @return \Illuminate\Http\Response
     */
    public function show(PunishRec $punishRec)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PunishRec  $punishRec
     * @return \Illuminate\Http\Response
     */
    public function edit(PunishRec $punishRec)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PunishRec  $punishRec
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PunishRec $punishRec)
    {
    	// dd($request->all());

        $records = PunishRec::where('employee_id',$request->employee_id)->get();
        foreach ($records as $key => $value) {
    	// dd($value);
        $updates = [
            'punishment_awarded' => $request->punishment_name[$key],
            'nature_of_offence' => $request->nature_offence[$key],
            'punished_by' => $request->punished_by[$key]
          ];

        $punishRec->where([
            'punishment_awarded' => $value->punishment_awarded,
            'nature_of_offence' => $value->nature_of_offence,
            'punished_by' => $value->punished_by,
            'employee_id'=>$request->employee_id])->update($updates);
        }
        $request->session()->flash('alert-success', 'Record Updated!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PunishRec  $punishRec
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {

        // dd($request->all());
          PunishRec::where('nature_of_offence', $request->nature_of_offence)->where('employee_id', $request->employee_id)->delete();
            $message = $request->session()->flash('alert-danger', 'Record Deletd!');
        
      
         return response()->json([
        'status' => 1, 
        'message' => $message
       ], 200 );
        // return redirect()->back();
    }
}
