<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Zipper;
use App\Employee;
use File;
use PDF;

class PDFController extends Controller
{
     public function printPDF(Request $request)
    {

  	 $data  =   Employee::with('employement_record','punishment_record','civil_qualification','tech_qualification')->where('employee_id',$request->employee_id)->first();

  	 $img = null;
if(!is_null($data->image)){

		$img_content = file_get_contents(public_path('/images/'.$data->image)); 
		$img = base64_encode($img_content); 
}
else{
			$img_content = file_get_contents(public_path('/images/employee-avi.jpg')); 
		$img = base64_encode($img_content); 
}


        $pdf = PDF::loadView('pdf_view',  ['data' => $data,'img'=>$img]); 
        return $pdf->stream(); 
        // return $pdf->download($request->employee_id.'.pdf');
    }


    public function download_pdf_all(Request $request){
      // var_dump($request->all());
      // die;
    ini_set('max_execution_time', 20000);
        $headers = ["Content-Type"=>"application/zip"];
        $zipper = new \Chumper\Zipper\Zipper;
        $currentDate = date("YmdHis");

// dd(realpath(public_path()));
    // dd($request->IDs);
foreach ($request->IDs as $id) {
  # code...
     $data  =   Employee::with('employement_record','punishment_record','civil_qualification','tech_qualification')->where('employee_id',$id)->first();

          $img = null;
          if(!is_null($data->image)){

              $img_content = file_get_contents(public_path('/images/'.$data->image)); 
              $img = base64_encode($img_content); 
          }
          else{
                $img_content = file_get_contents(public_path('/images/employee-avi.jpg')); 
              $img = base64_encode($img_content); 
          }


        $pdf = PDF::loadView('pdf_view',  ['data' => $data,'img'=>$img]); 
        
        file_put_contents(public_path().'/documents/'.$id.'.pdf', $pdf->output());
// dd(date("YmdHis"));
        // $file->move(public_path('/documents'), $file);


        $fileName = public_path()."/documents/".$id.'.pdf'; // name of zip
        $zipName = 'records-'.$currentDate.'.zip'; // name of zip
        $zipper->make(public_path().'/documents/records-'.$currentDate.'.zip')->add($fileName); //files to be zipped
        $zipper->close();

          if(File::exists($fileName)) {
              File::delete($fileName);
          }
        // return $pdf->download($request->employee_id.'.pdf');
    }
        // $downloadData = [
        //   'zipFile' => $zipName,
        //   'headers' => $headers,
        //   'url' => public_path('documents/').$zipName
        // ];
        // return response()->download(public_path().'/documents/'.$zipName , $zipName, $headers);

        // dd(route('download_zip'));
        // return response()->json(['status'=>1,'url'=> public_path('documents/').$zipName]);

        return route('download_zip', ['url' => public_path('documents/').$zipName]);


}


public function pdf_merged(Request $request)
{
    ini_set('max_execution_time', 20000);
      $currentDate = date("YmdHis");
      $pdfMerger = new \LynX39\LaraPdfMerger\PdfManage;
      foreach ($request->IDs as $id) {

     $data  =   Employee::with('employement_record','punishment_record','civil_qualification','tech_qualification')->where('employee_id',$id)->first();

          $img = null;
          if(!is_null($data->image)){

              $img_content = file_get_contents(public_path('/images/'.$data->image)); 
              $img = base64_encode($img_content); 
          }
          else{
                $img_content = file_get_contents(public_path('/images/employee-avi.jpg')); 
              $img = base64_encode($img_content); 
          }

        $pdf = PDF::loadView('pdf_view',  ['data' => $data,'img'=>$img]); 
        
        file_put_contents(public_path().'/documents/'.$id.'.pdf', $pdf->output());

        $fileName = public_path()."/documents/".$id.'.pdf'; 
        
        $pdfMerger->addPDF($fileName, 'all');
        
        $mergedName = public_path()."/documents/merged/".$currentDate.'.pdf'; 
        $pdfMerger->merge('file', $mergedName);
         
         

        }
        foreach ($request->IDs as $id) {
        $fileName = public_path()."/documents/".$id.'.pdf'; 

           if(File::exists($fileName)) {
              File::delete($fileName);
          }
        }

        return route('download_merged', ['url' => $mergedName]);
    
}


}
