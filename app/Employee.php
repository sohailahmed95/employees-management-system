<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Employee extends Model
{
	    use SoftDeletes;

	protected $table = 'employee_info';
      protected $fillable = [
        'employee_id','rank', 'bps', 'name','email','image','date_of_birth','cnic','domicile','blood_group','marital_status','no_of_children','present_address','permanent_address','date_of_enrolment','present_unit_id','date_of_present_rank','utilization_of_posting','completion_of_25yrs','superannuation','qual_at_time_enrol','division','created_at','updated_at'
    ];

  
   public function employement_record()
    {
    	return $this->hasMany('App\EmpRec','employee_id','employee_id');
    } 

    public function punishment_record()
    {
    	return $this->hasMany('App\PunishRec','employee_id','employee_id');
    }  

    public function civil_qualification()
    {
    	return $this->hasMany('App\CivQual','employee_id','employee_id');
    } 

    public function tech_qualification()
    {
    	return $this->hasMany('App\TechQual','employee_id','employee_id');
    }

    public function present_unit(){
    	return $this->hasOne('App\Unit','id','present_unit_id');
    }

     public function units(){
    	return $this->hasMany('App\Unit','id','unit_id');
    }
}
