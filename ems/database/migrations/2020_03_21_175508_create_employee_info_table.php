<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id')->nullable();
            $table->string('rank')->nullable();
            $table->string('bps')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('image')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('cnic')->nullable();
            $table->string('domicile')->nullable();
            $table->string('blood_group')->nullable();
            $table->string('marital_status')->nullable();
            $table->integer('no_of_children')->nullable();
            $table->string('present_address')->nullable();
            $table->string('permanent_address')->nullable();
            $table->string('date_of_enrolment')->nullable();

            $table->integer('present_unit_id')->unsigned()->nullable();

            $table->string('date_of_present_rank')->nullable();
            $table->string('utilization_of_posting')->nullable();
            $table->string('completion_of_25yrs')->nullable();
            $table->string('superannuation')->nullable();
            $table->string('qual_at_time_enrol')->nullable();
            $table->string('division')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('present_unit_id')
                 ->references('id')->on('unit')
                 ->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_info');
    }
}
