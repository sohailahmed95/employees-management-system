<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class EmpRec extends Model
{
    use SoftDeletes;

	protected $table = 'employement_record';
      protected $fillable = [
        'unit_id','employee_id','from','to'
    ];

      public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

 

      public function units(){
    	return $this->hasMany('App\Unit','id','unit_id');
    }

   
}
