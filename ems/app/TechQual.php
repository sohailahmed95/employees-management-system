<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TechQual extends Model
{
     use SoftDeletes;

	protected $table = 'technical_qualification';
      protected $fillable = [
        'employee_id','course', 'division','institute','from','to'
    ];

      public function employee()
    {
        return $this->belongsTo('App\Employee','employee_id','employee_id');
    }
}
