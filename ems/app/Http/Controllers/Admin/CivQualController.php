<?php

namespace App\Http\Controllers\Admin;

use App\CivQual;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CivQualController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = CivQual::where('employee_id',$request->employee_id)->get();
         // dd($data);
        return view('employee.edit.civqual',compact('data'));  
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
         $data = CivQual::with('employee')->where('employee_id',$request->employee_id)->get();
        // $units = Unit::all();
                
        return view('employee.add.civqual',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, CivQual $civQual)
    {
        // $records = EmpRec::where('employee_id',$request->employee_id)->get();

        // foreach ($request->all() as $key => $value) {
     
        foreach ($request->c_course_name as $key => $value) {
        $data = [
            'employee_id' => $request->employee_id,
            'course' => $request->c_course_name[$key],
            'division' => $request->c_division[$key],
           
          ];

        $civQual->create($data);
        }

        $request->session()->flash('alert-success', 'Record Added!');
   


        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CivQual  $civQual
     * @return \Illuminate\Http\Response
     */
    public function show(CivQual $civQual)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CivQual  $civQual
     * @return \Illuminate\Http\Response
     */
    public function edit(CivQual $civQual)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CivQual  $civQual
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CivQual $civQual)
    {
         $records = CivQual::where('employee_id',$request->employee_id)->get();

        foreach ($records as $key => $value) {
        $updates = [
            'course' => $request->c_course_name[$key],
            'division' => $request->c_division[$key]
          ];
        $civQual->where(['course' => $value->course , 'division' => $value->division  ,'employee_id'=>$request->employee_id])->update($updates);
        }
        $request->session()->flash('alert-success', 'Record Updated!');
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CivQual  $civQual
     * @return \Illuminate\Http\Response
     */
   public function destroy(Request $request)
    {

       CivQual::where('course', $request->course)->where('division', $request->division)->delete();
            $message = $request->session()->flash('alert-danger', 'Record Deletd!');
        
      
         return response()->json([
        'status' => 1, 
        'message' => $message
       ], 200 );
        // return redirect()->back();
    }
}
