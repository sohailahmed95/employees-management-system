<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use App\Employee;
use App\EmpRec;
use App\Unit;


class EmployementController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
  		 $data = EmpRec::with('units')->where('employee_id',$request->employee_id)->get();
         // dd($data);

        $units = Unit::all();

        return view('employee.edit.employement',compact('data','units'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
         $data = EmpRec::with('units')->where('employee_id',$request->employee_id)->get();

        $units = Unit::all();
                
        return view('employee.add.employement',compact('data','units'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,EmpRec $empRec)
    {
        // dd($request->emp_unit_id[$key]);
        // $records = EmpRec::where('employee_id',$request->employee_id)->get();

        // foreach ($request->all() as $key => $value) {
     
        foreach ($request->emp_unit_id as $key => $value) {
        $emp_rec = [
            'employee_id' => $request->employee_id,
            'unit_id' => $request->emp_unit_id[$key],
            'from' => $request->emp_unitFromDate[$key],
            'to' => $request->emp_unitToDate[$key],
           
          ];

        $empRec->create($emp_rec);
        }

        $request->session()->flash('alert-success', 'Record Added!');
   


        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmpRec $empRec)
    {

        $records = EmpRec::where('employee_id',$request->employee_id)->get();

        foreach ($records as $key => $value) {
        $updates = [
            'unit_id' => $request->emp_unit_id[$key],
            'from' => $request->emp_unitFromDate[$key],
            'to' => $request->emp_unitToDate[$key]
          ];
        $empRec->where(['unit_id'=>$value->unit_id,'employee_id'=>$request->employee_id])->update($updates);
        }
        $request->session()->flash('alert-success', 'Record Updated!');
        
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
         EmpRec::where('from', $request->from)->where('to', $request->to)->delete();
            $message =  $request->session()->flash('alert-danger', 'Record Deletd!');
        
         return response()->json([
        'status' => 1, 
        'message' => $message
       ], 200 );
       
    }
}
