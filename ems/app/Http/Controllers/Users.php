<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class Users extends Controller
{
      public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

   		$users = User::all();
        return view('admin.users',compact('users'));
       	// return view('layouts.admin.users');
    }

       public function user_activate(Request $request)
    {   var_dump ($request);
        die();
        $user = User::where('id',$request->id)->first();
        if ($user->is_active  == 0) {
        
          $user->is_active = 1;
          $user->save();
        } 
      
        return response()->json(['status'=>true,'data'=>$user]);
    }       

      public function user_deactivate(Request $request)
    {   var_dump ($request);
        die();
        $user = User::where('id',$request->id)->first();
        if ($user->is_active  == 1) {
        
          $user->is_active = 0;
          $user->save();
        } 

        return response()->json(['status'=>true,'data'=>$user]);
    }  
}
