<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CivQual extends Model
{
     use SoftDeletes;

	protected $table = 'civil_qualification';
      protected $fillable = [
        'employee_id','course', 'division'
    ];

       public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
}
