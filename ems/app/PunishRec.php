<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class PunishRec extends Model
{
   use SoftDeletes;

	protected $table = 'punishment_record';
      protected $fillable = [
        'nature_of_offence','punishment_awarded','punished_by','employee_id'
    ];

        public function employee()
    {
        return $this->belongsTo('App\Employee');
    }
}
