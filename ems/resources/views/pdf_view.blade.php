<!DOCTYPE html>
<html >
	<head>
	</head>
	<style>
		  @page {
      size: 8.27in 11.69in;
      margin: 30px 60px 20px 60px;
      }
      body{
      	font-size: 13px;
      }
      hr{
      	height:1px;border-width:0;color:#e4e7ed;background-color:#e4e7ed
      }

      .main-heading{
      	text-align: center;
      }
      .section-heading{
      	font-size: 14px
      }
		.section-th{
		font-size: 12px;

		padding:  8px 8px
		    color: #8898aa;
    border-color: #e9ecef;
    background-color: #f6f9fc;
		}
		.field-value{
			font-size: 12px;
			text-align: center;
		}
		.profile-img{
			position: absolute;

		/*	width:150px;
			 height:150px;*/
			 /*float: right;*/
			 /*padding: 10px*/
			 top: 150px;
			 left: 0;

		}
.top-field-title{
	text-decoration: underline;
}
		.top-field-value{
			margin-left: 100px;
			/*width: 100%*/
			/*text-align: right;*/
		}
		.img-thumbnail{
	width: 180px;
    height: 180px;
    padding: .25rem;
    border: 1px solid #dee2e6;
    border-radius: .375rem;
    background-color: #f8f9fe;
    box-shadow: 0 1px 2px rgba(0, 0, 0, .075);
		}
	</style>
	<body>
		<table width="100%">
			<thead></thead>
			<tbody>
				<tr><td><h1 class="main-heading">Employee Profile</h1></td></tr>
			</tbody>
		</table>
		<table>
			<thead></thead>
			<tbody>
				<tr><td><h3 class="section-heading">PERSONAL INFORMATION</h3></td></tr>
			</tbody>
		</table>
		<table width="100%">
			<thead>
				
			</thead>
			<tbody>
				
				<tr  class="top-sec-tr">
					<td >
						<tr >
							
							<td  >
  						<img class="profile-img img-thumbnail"  src="data:image/png;base64,{{$img}}" alt="Image Not Available">
					</td>
  						<td  class="top-field-title">Employee ID:</td>
							<td class="top-field-value">{{$data->employee_id ?? 'N/A'}}</td>
						
						</tr>

					</td>
				</tr>

				<tr  class="top-sec-tr">
					<td >
						<tr >
							<td width="33.33%" class=""></td>
							<td width="33.33%" class="top-field-title">Rank:</td>
							<td width="33.33%" class="top-field-value">{{$data->rank ?? 'N/A'}}</td>
						</tr>
					</td>
				</tr>
				<tr  class="top-sec-tr">
					<td >
						<tr >
							<td width="33.33%" class=""></td>

							<td  width="33.33%" class="top-field-title">BPS:</td>
							<td  width="33.33%" class="top-field-value">{{$data->bps ?? 'N/A'}}</td>
							
							
						</tr>
					</td>
				</tr>
					<tr  class="top-sec-tr">
					<td >
						<tr >
							<td width="33.33%" class=""></td>

							<td width="33.33%" class="top-field-title">CNIC#:</td>
							<td width="33.33%" class="top-field-value">{{$data->cnic ?? 'N/A'}}</td>
						</tr>
					</td>
				</tr>
				<tr  class="top-sec-tr">
					<td >
						<tr >
							<td width="33.33%" class=""></td>

							<td width="33.33%" class="top-field-title">Full Name:</td>
							<td width="33.33%" class="top-field-value">{{$data->name ?? 'N/A'}}</td>
						
						</tr>
					</td>
				</tr>
				<tr  class="top-sec-tr">
					<td >
						<tr >
							<td width="33.33%" class=""></td>
							
							<td width="33.33%" class="top-field-title">Date of Birth:</td>
							<td width="33.33%" class="top-field-value">{{$data->date_of_birth ?? 'N/A'}}</td>
						</tr>
					</td>
				</tr>
				<tr  class="top-sec-tr">
					<td >
						<tr >
							<td width="33.33%" class=""></td>

							<td width="33.33%" class="top-field-title">Blood Group:</td>
							<td width="33.33%" class="top-field-value">{{$data->blood_group ?? 'N/A'}}</td>
							
						</tr>
					</td>
				</tr>

				<tr  class="top-sec-tr">
					<td >
						<tr >
							<td width="33.33%" class=""></td>

							<td width="33.33%" class="top-field-title">Marital Status:</td>
							<td width="33.33%" class="top-field-value">{{$data->marital_status ?? 'N/A'}}</td>							
						</tr>
					</td>
				</tr>

					<tr  class="top-sec-tr">
					<td >
						<tr >
							<td width="33.33%" class=""></td>
							<td width="33.33%" class="top-field-title">Domicile:</td>
							<td width="33.33%" class="top-field-value">{{$data->domicile ?? 'N/A'}}</td>
							
						</tr>
					</td>
				</tr>
				<tr  class="top-sec-tr">
					<td >
						<tr >
							<td class="top-field-title">Email Address:</td>
							<td class="top-field-value">{{$data->email ?? 'N/A'}}</td>
							<td class="top-field-title">No. of Children:</td>
							<td class="top-field-value">{{$data->no_of_children ?? 'N/A'}}</td>
							
							
						</tr>
					</td>
				</tr class="top-sec-tr">
				<tr >
					<td >
						<tr >
							<td class="top-field-title">Present Address:</td>
							<td colspan="4" class="top-field-value">{{$data->present_address ?? 'N/A'}}</td>
							
						</tr>
					</td>
				</tr>
				<tr  class="top-sec-tr">
					<td >
						<tr >
							
							<td class="top-field-title">Permanent Address:</td>
							<td colspan="4" class="top-field-value">{{$data->permanent_address ?? 'N/A'}}</td>
							
							
						</tr>
					</td>
				</tr>
			</tbody>
		</table>
		<hr>
		<table>
			<thead></thead>
			<tbody>
				<tr><td><h3 class="section-heading">EMPLOYEMENT INFORMATION</h3></td></tr>
			</tbody>
		</table>

			<table>
			<thead>
				<tr>
					<th scope="col" class="section-th" data-sort="unit">#</th>
                    <th scope="col" class="section-th" data-sort="unit">Date of Enrolment</th>
                    <th scope="col" class="section-th" data-sort="from">Present Unit</th>
                    <th scope="col" class="section-th" data-sort="from">Date of Present Rank</th>
                    <th scope="col" class="section-th" data-sort="from">Utilization of Posting</th>
                    <th scope="col" class="section-th" data-sort="from">Completion of 25 Years</th>
                    <th scope="col" class="section-th" data-sort="from">Superannuation</th>
                </tr>
			</thead>
			<tbody>
				<tr style="background-color: #f9f9f9">
                   <td class="field-value"></td>
                   <td class="field-value">{{ $data->date_of_enrolment ?? 'N/A'}}</td>
                   <td class="field-value">{{ $data->present_unit_id ?? 'N/A'}}</td>
                   <td class="field-value">{{ \Carbon\Carbon::parse($data->date_of_present_rank)->format('Y-m-d') ?? 'N/A' }}</td>
                   <td class="field-value">{{ $data->utilization_of_posting ?? 'N/A'}}</td>
                   <td class="field-value">{{ $data->completion_of_25yrs ?? 'N/A'}}</td>
                   <td class="field-value">{{ $data->superannuation ?? 'N/A'}}</td>
                  
                 </tr>
			</tbody>
		</table>
		<hr>
			<table>
			<thead></thead>
			<tbody>
				<tr><td><h3 class="section-heading">QUALIFICATION AT TIME OF ENROLMENT</h3></td></tr>
			</tbody>
		</table>
		<table width="100%">
			 <thead>
                  <tr>
                    <th scope="col" class="section-th" data-sort="unit">Course Name</th>
                    <th scope="col" class="section-th" data-sort="from">Division</th>
                   
                  </tr>
                </thead>
                <tbody class="list">
                  <tr style="background-color: #f9f9f9">
                    <td class="field-value">{{ $data->qual_at_time_enrol ?? 'N/A' }}</td>
                    <td class="field-value">{{ $data->division ?? 'N/A' }}</td>
                  </tr>
                </tbody>
		</table>
<hr>
		<table>
			<thead></thead>
			<tbody>
				<tr><td><h3 class="section-heading">EMPLOYEMENT RECORD</h3></td></tr>
			</tbody>
		</table>

		<table width="100%">
			  <thead class="thead-light">
                  <tr>
                    <th scope="col" class="section-th" data-sort="unit">#</th>
                    <th scope="col" class="section-th" data-sort="unit">Unit</th>
                    <th scope="col" class="section-th" data-sort="from">From</th>
                    <th scope="col" class="section-th" data-sort="to">To</th>
                   
                  </tr>
                </thead>
                <tbody class="list">
                	@php($i = 1)
                  @foreach($data->employement_record as $item)
                    {{-- @php(dd($item->units)) --}}
                    <?php
                    $from = $item->from != null ? \Carbon\Carbon::parse($item->from)->format('Y-m-d') : 'N/A' ;
                    $to = $item->to != null ? \Carbon\Carbon::parse($item->to)->format('Y-m-d') : null;
                    ?>
                      @foreach($item->units as $unit)

                      <tr style="background-color: #f9f9f9">
                        <td class="field-value">{{ $i++ }}</td>
                        <td class="field-value">{{ $unit->name ?? 'N/A' }}</td>
                        <td class="field-value">{{ $from }}</td>
                    	<td class="field-value">{{ isset($from) && $to == null ? 'till present' : $to }} </td>
                       
                  
                      </tr>
                       @endforeach

                @endforeach
              
               
                </tbody>
		</table>
		<hr>
		<table>
			<thead></thead>
			<tbody>
				<tr><td><h3 class="section-heading">CIVIL QUALIFICATION</h3></td></tr>
			</tbody>
		</table>

<table width="100%">
	 <thead >
                  <tr>
                    <th scope="col" class="section-th" data-sort="index">#</th>
                    <th scope="col" class="section-th" data-sort="course">Course Name</th>
                    <th scope="col" class="section-th" data-sort="division">Division</th>
                   
                  </tr>
                </thead>
                <tbody class="list">
                  @php($i = 1)
                  @foreach($data->civil_qualification as $item)
                    {{-- @php(dd($item->course)) --}}
                  <tr style="background-color: #f9f9f9">
                    <td class="field-value">{{ $i++}}</td>
                    <td class="field-value">{{ $item->course ?? 'N/A'}}</td>
                   <td class="field-value" >{{ $item->division ?? 'N/A'}}</td>
              
                  </tr>
                @endforeach
              
               
                </tbody>
</table>
		
<br><br><br>
		<table>
			<thead></thead>
			<tbody>
				<tr><td><h3 class="section-heading">TECHNICAL QUALIFICATION</h3></td></tr>
			</tbody>
		</table>


		  <table width="100%">
                <thead >
                  <tr>
                    <th scope="col" class="section-th" data-sort="index">#</th>
                    <th scope="col" class="section-th" data-sort="course">Course Name</th>
                    <th scope="col" class="section-th" data-sort="division">Division</th>
                    <th scope="col" class="section-th" data-sort="institue">Institue</th>
                    <th scope="col" class="section-th" data-sort="from">From</th>
                    <th scope="col" class="section-th" data-sort="to">To</th>
                   
                  </tr>
                </thead>
                <tbody class="list">
                  @php($i = 1)
                  @foreach($data->tech_qualification as $item)
                    {{-- @php(dd($item->course)) --}}
                     <?php
                    $from = $item->from != null ? \Carbon\Carbon::parse($item->from)->format('Y-m-d') : 'N/A' ;
                    $to = $item->to != null ? \Carbon\Carbon::parse($item->to)->format('Y-m-d') : null;
                    ?>
                  <tr style="background-color: #f9f9f9">
                    <td class="field-value">{{ $i++}}</td>
                    <td class="field-value">{{ $item->course ?? 'N/A'}}</td>
                    <td class="field-value">{{ $item->division ?? 'N/A'}}</td>
                    <td class="field-value">{{ $item->institute ?? 'N/A'}}</td>
               <td class="field-value">{{ $from }}</td>
                    <td class="field-value">{{ isset($from) && $to == null ? 'till present' : $to }} </td>
                       
                  </tr>
                @endforeach
                </tbody>
              </table>
<hr>
              	<table>
			<thead></thead>
			<tbody>
				<tr><td><h3 class="section-heading">PUNISHMENT RECORD</h3></td></tr>
			</tbody>
		</table>
 <table  width="100%">
                <thead >
                  <tr>
                    <th scope="col" class="section-th" data-sort="index">#</th>
                    <th scope="col" class="section-th" data-sort="division">Nature Of Offence</th>
                    <th scope="col" class="section-th" data-sort="course">Punishment Awarded</th>
                    <th scope="col" class="section-th" data-sort="institue">Punished By</th>
                   
                  </tr>
                </thead>
                <tbody class="list">
                  @php($i = 1)
                  @foreach($data->punishment_record as $item)
                    {{-- @php(dd($item->course)) --}}
                  <tr style="background-color: #f9f9f9">
                    <td class="field-value">{{ $i++}}</td>
                    <td class="field-value">{{ $item->nature_of_offence ?? 'N/A'}}</td>
                    <td class="field-value">{{ $item->punishment_awarded ?? 'N/A'}}</td>
                    <td class="field-value">{{ $item->punished_by ?? 'N/A'}}</td>
              
                  </tr>
                @endforeach
                </tbody>
              </table>
	
</body>
</html>