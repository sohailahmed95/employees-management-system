@extends('layouts.app')
@section('content')
<div class="container">
   <div class="row">
        <div class="col-xl-12 order-xl-1">
          <div class="card bg-form">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Add Civil Qualification</h3>
                </div>
                <div class="col-4 text-right">
                  <a href="{{url()->previous()}}" title="Go To Previous Page" class="btn btn-sm btn-primary">go back</a>
                </div>
              </div>
            </div>
            {{-- @php(dd($data)) --}}
            <div class="card-body">
                  <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}"><strong>{{ Session::get('alert-' . $msg) }}</strong> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                        @endforeach
                      </div>
              <form id="employeeForm" action="{{route('civQual.save')}}" method="post">
                  {{ csrf_field() }}

                <!-- Address -->
                {{-- <h6 class="heading-small text-secondary mb-4"></h6> --}}
                  {{-- <div class="techQual"> --}}
                  
                  {{-- <h3 class="text-white" style="  text-decoration: underline;">Record - {{ $i++}}</h3> --}}
                  <input type="text" hidden name="employee_id" id="employee_id" value="{{$data[0]->employee_id}}">
                  <div id="add_civ_qual_box">
  
                    <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="c_course_name">Course Name</label>
                        <input id="c_course_name" required name="c_course_name[]" value="" class="form-control"  type="text">
                        {{-- <div id="c_course_name_inputs"></div> --}}
                      </div>
                    </div>
                  {{-- </div> --}}
                  {{-- <div class="row"> --}}
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="c_division">Division</label>
                        <input type="text" id="c_division" required name="c_division[]" value="" class="form-control" >
                        {{-- <div id="c_division_inputs"></div> --}}
                      </div>
                    </div>
                    </div>
                    </div>
                 <div class="row">
                    <div class="col-md-12">
   <input type="submit" value="Save" class="float-right btn btn-sm btn-success ">
                      <input type="button" value="Add More" id="add_more" class="float-right btn btn-sm btn-success mr-2">                    </div>
                 
                    </div>
            {{-- </div> --}}
          </div>
        </div>
      </div>
      </div>
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>

<script>

$('#add_more').click(function(event) {

  var html = $('#add_civ_qual_box .row:first').html();
  // console.log(html);
 // $('.techQual').append('<hr>');
 $('#add_civ_qual_box').append(`<div class="row pt-5">${html}<div class="col-md-12"><a class="btn bttt btn-sm btn-danger" onclick="$(this).parent('.col-md-12').parent('.row').remove()">Remove</a></div></div>`);
});

</script>
@endsection
