@extends('layouts.app')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.datetimepicker.css') }}">

<div class="container">
   <div class="row">
        <div class="col-xl-12 order-xl-1">
          <div class="card bg-form">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Add Technical Qualification - Employee Name:</h3>
                </div>
                <div class="col-4 text-right">
                  <a href="{{url()->previous()}}" title="Go To Previous Page" class="btn btn-sm btn-primary">go back</a>
                </div>
              </div>
            </div>
            {{-- @php(dd($data)) --}}
            <div class="card-body">
                  <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}"><strong>{{ Session::get('alert-' . $msg) }}</strong> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                        @endforeach
                      </div>
              <form id="employeeForm" action="{{route('techqual.save')}}" method="post" >
                  {{ csrf_field() }}

                <!-- Address -->
                {{-- <h6 class="heading-small text-secondary mb-4"></h6> --}}
                  {{-- @foreach($data as $item) --}}
                  <input type="text" hidden name="employee_id" id="employee_id" value="{{$data[0]->employee_id}}">
<div id="add_tech_qual_box">
  
                 <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="t_course_name">Course Name</label>
                        <input id="t_course_name" name="t_course_name[]" required value="" class="form-control" placeholder="N/A"  type="text">
                        {{-- <div id="t_course_name_inputs"></div> --}}
                      </div>
                    </div>
                     <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="t_division">Division</label>
                        <input type="text" id="t_division" name="t_division[]" required value="" class="form-control" placeholder="N/A" >
                        {{-- <div id="t_institue_inputs"></div> --}}
                      </div>
                    </div>
              
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="t_institue">Institue</label>
                        <input type="text" id="t_institue" name="t_institue[]" required value="" class="form-control" placeholder="N/A" >
                        {{-- <div id="t_institue_inputs"></div> --}}
                      </div>
                    </div>
                  
                      <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="t_fromDate">From</label>
                        <input type="text" id="t_fromDate" name="t_fromDate[]" class="form-control datetimepicker" placeholder="N/A" required value="">
                    </div>
                      </div>
                      <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="t_toDate">To <small>(Leave blank if still in progress)</small></label>
                        <input type="text" id="t_toDate" name="t_toDate[]" class="form-control datetimepicker" placeholder="N/A" required value="">
                    </div>
                      </div>
                      
                    </div>
                    </div>
                    {{-- @endforeach --}}

                        <div class="row">
                    <div class="col-md-12">
   <input type="submit" value="Save" class="float-right btn btn-sm btn-success ">
                      <input type="button" value="Add More" id="add_more" class="float-right btn btn-sm btn-success mr-2">                    </div>
                 
                    </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery.datetimepicker.full.js') }}"></script>
             
<script>
$(document).ready(function() {
dtp();

  });  

    function dtp() {
       var today = new Date('Y-m-d');
    $('.datetimepicker').datetimepicker({
    timepicker:false,
    format:'Y-m-d'
    // maxDateTime: today

  });  
    }

// });

$('#add_more').click(function(event) {

  var html = $('#add_tech_qual_box .row:first').html();
  // console.log(html);
 // $('.techQual').append('<hr>');
 $('#add_tech_qual_box').append(`<div class="row pt-5">${html}<div class="col-md-12"><a class="btn bttt btn-sm btn-danger" onclick="$(this).parent('.col-md-12').parent('.row').remove()">Remove</a></div></div>`);
dtp();
});

</script>
@endsection