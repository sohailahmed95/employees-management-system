@extends('layouts.app')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.datetimepicker.css') }}">

<div class="container">
   <div class="row">
        <div class="col-xl-12 order-xl-1">
          <div class="card bg-form">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Add Punishment Record</h3>
                </div>
                <div class="col-4 text-right">
                  <a href="{{url()->previous()}}" title="Go To Previous Page" class="btn btn-sm btn-primary">go back</a>
                </div>
              </div>
            </div>
            {{-- @php(dd($data)) --}}
            <div class="card-body">
                  <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}"><strong>{{ Session::get('alert-' . $msg) }}</strong> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                        @endforeach
                      </div>
              <form id="employeeForm" action="{{route('punishrec.save')}}" method="post">
                  {{ csrf_field() }}

                <!-- Address -->
                {{-- <h6 class="heading-small text-secondary mb-4"></h6> --}}
                  {{-- <h3 class="text-white" style="  text-decoration: underline;">Record # {{ $i++}}</h3> --}}
                  <input type="text" hidden name="employee_id" id="employee_id" value="{{$data[0]->employee_id}}">
<div id="add_pun_rec_box">

                    {{-- <h6 class="heading-small text-secondary mb-4">Punishment Record</h6> --}}
                  <div class="row">
                     <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="punishment_name">Punishment Awarded</label>
                        <input type="text" id="punishment_name" required name="punishment_name[]" class="form-control" placeholder="N/A " value="" >
                        {{-- <div id="t_institue_inputs"></div> --}}
                      </div>
                    </div>
                
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="nature_offence">Nature Of Offence</label>
                        <input id="nature_offence" required name="nature_offence[]" class="form-control" placeholder="N/A" value=""  type="text">
                        {{-- <div id="t_course_name_inputs"></div> --}}
                      </div>
                    </div>
                      <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="punished_by">Punished By</label>
                           <select class="form-control" id="punished_by" required name="punished_by[]">
                   
                            <option value="" selected hidden>Select</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                           </select>
                        {{-- <input id="utilization_of_posting" name="utilization_of_posting" class="form-control datetimepicker" placeholder="2020-04-01" type="text"> --}}
                      </div>
                    </div>
                  </div>
                  </div>
                        <div class="row">
                    <div class="col-md-12">
   <input type="submit" value="Save" class="float-right btn btn-sm btn-success ">
                      <input type="button" value="Add More" id="add_more" class="float-right btn btn-sm btn-success mr-2">                    </div>
                 
                    </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
             
<script>

$('#add_more').click(function(event) {

  var html = $('#add_pun_rec_box .row:first').html();
  // console.log(html);
 // $('.techQual').append('<hr>');
 $('#add_pun_rec_box').append(`<div class="row pt-5">${html}<div class="col-md-12"><a class="btn bttt btn-sm btn-danger" onclick="$(this).parent('.col-md-12').parent('.row').remove()">Remove</a></div></div>`);
dtp();
});

</script>
@endsection