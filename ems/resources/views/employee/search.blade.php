@extends('layouts.app')
@section('content')
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css"> --}}
<style>
.row{
margin-left:0px;
margin-right:0px;
}
#wrapper {
/*padding-left: 70px;*/
transition: all .4s ease 0s;
height: 100%
}
#search_wrapper {
/*    margin-left: -150px;
/*left: 70px;*/
/*right: -90px;
width: 150px;
background: #222;
position: fixed;
height: 100%;
z-index: 10000;*/
/*transition: all .4s ease 0s;*/
/*transition:max-height 2s ease*/
}
.sidebar-nav {
display: block;
float: left;
width: 150px;
list-style: none;
margin: 0;
padding: 0;
}
#page-content-wrapper {
padding-left: 0;
margin-left: 0;
width: 100%;
height: auto;
}
#wrapper.active {
/*padding-left: 150px;*/
padding-top: 330px;
}
#wrapper.active #search_wrapper {
right: 50px;
}
#page-content-wrapper {
width: 100%;
}
#sidebar_menu li a, .sidebar-nav li a {
color: #999;
display: block;
float: left;
text-decoration: none;
width: 150px;
background: #252525;
border-top: 1px solid #373737;
border-bottom: 1px solid #1A1A1A;
-webkit-transition: background .5s;
-moz-transition: background .5s;
-o-transition: background .5s;
-ms-transition: background .5s;
transition: background .5s;
}
.sidebar_name {
padding-top: 25px;
color: #fff;
opacity: .7;
}
.sidebar-nav li {
line-height: 40px;
text-indent: 20px;
}
.sidebar-nav li a {
color: #999999;
display: block;
text-decoration: none;
}
.sidebar-nav li a:hover {
color: #fff;
background: rgba(255,255,255,0.2);
text-decoration: none;
}
.sidebar-nav li a:active,
.sidebar-nav li a:focus {
text-decoration: none;
}
.sidebar-nav > .sidebar-brand {
height: 65px;
line-height: 60px;
font-size: 18px;
}
.sidebar-nav > .sidebar-brand a {
color: #999999;
}
.sidebar-nav > .sidebar-brand a:hover {
color: #fff;
background: none;
}
#main_icon
{
float:right;
padding-right: 65px;
padding-top:20px;
}
.sub_icon
{
float:right;
padding-right: 65px;
padding-top:10px;
}
.content-header {
height: 65px;
line-height: 65px;
}
.content-header h1 {
margin: 0;
margin-left: 20px;
line-height: 65px;
display: inline-block;
}
@media (max-width:767px) {
#wrapper {
padding-left: 70px;
transition: all .4s ease 0s;
}
#search_wrapper {
left: 70px;
}
#wrapper.active {
/*padding-left: 150px;*/
padding-top: 150px;
}
#wrapper.active #search_wrapper {
/*    left: 150px;
width: 150px;*/
transition: all .4s ease 0s;
}
}
.search-bar .dropdown-menu {
display: block;
}
.search-bar .dropdown-menu {
max-height: 0;
opacity: 0;
overflow: hidden;
transition: max-height .7s;
}
.search-bar .dropdown-menu.show {
opacity: 1;
max-height: 500px;
}
.search-bar .search_form-group{
margin: 0;
}
</style>
{{-- <div id="sidebar-wrapper">
  <ul id="sidebar_menu" class="sidebar-nav">
    <li class="sidebar-brand"><a id="menu-toggle" href="#">Menu<span id="main_icon" class="glyphicon glyphicon-align-justify"></span></a></li>
  </ul>
  <ul class="sidebar-nav" id="sidebar">
    <li><a>Link1<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
    <li><a>link2<span class="sub_icon glyphicon glyphicon-link"></span></a></li>
  </ul>
</div> --}}
<div class="container">
  <div class="row">
    <div class="col-xl-12 order-xl-1">
      
      <div class="card bg-form" id="page-content-wrapper">
        <div class="card-header">
          <div class="row align-items-center">
            <div class="col-7">
              <h3 class="mb-0">All Employees Record</h3>
            </div>
            <div class="col-5 text-right">
              <a href="{{url()->previous()}}" title="Go To Previous Page" class="btn btn-sm btn-primary">go back</a>
              <a href="" id="download_all" class="btn btn-sm btn-danger" title="Download Current List as ZIP">Download All <svg width="15px" height="15px" aria-hidden="true" focusable="false" data-prefix="far" data-icon="file-pdf" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-file-pdf fa-w-12 fa-2x"><path fill="currentColor" d="M369.9 97.9L286 14C277 5 264.8-.1 252.1-.1H48C21.5 0 0 21.5 0 48v416c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48V131.9c0-12.7-5.1-25-14.1-34zM332.1 128H256V51.9l76.1 76.1zM48 464V48h160v104c0 13.3 10.7 24 24 24h104v288H48zm250.2-143.7c-12.2-12-47-8.7-64.4-6.5-17.2-10.5-28.7-25-36.8-46.3 3.9-16.1 10.1-40.6 5.4-56-4.2-26.2-37.8-23.6-42.6-5.9-4.4 16.1-.4 38.5 7 67.1-10 23.9-24.9 56-35.4 74.4-20 10.3-47 26.2-51 46.2-3.3 15.8 26 55.2 76.1-31.2 22.4-7.4 46.8-16.5 68.4-20.1 18.9 10.2 41 17 55.8 17 25.5 0 28-28.2 17.5-38.7zm-198.1 77.8c5.1-13.7 24.5-29.5 30.4-35-19 30.3-30.4 35.7-30.4 35zm81.6-190.6c7.4 0 6.7 32.1 1.8 40.8-4.4-13.9-4.3-40.8-1.8-40.8zm-24.4 136.6c9.7-16.9 18-37 24.7-54.7 8.3 15.1 18.9 27.2 30.1 35.5-20.8 4.3-38.9 13.1-54.8 19.2zm131.6-5s-5 6-37.3-7.8c35.1-2.6 40.9 5.4 37.3 7.8z" class=""></path></svg></a>
               <a href="" id="download_merged" class="btn btn-sm btn-dark" title="Download Current List as Merged PDF">Download Merged <svg width="15px" height="15px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="file-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-file-alt fa-w-12 fa-2x"><path fill="currentColor" d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z" class=""></path></svg></a>
                 <a href="" id="download_merged" class="btn btn-sm btn-dark" title="Download Current List as Merged PDF">Selective <svg width="15px" height="15px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="file-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-file-alt fa-w-12 fa-2x"><path fill="currentColor" d="M224 136V0H24C10.7 0 0 10.7 0 24v464c0 13.3 10.7 24 24 24h336c13.3 0 24-10.7 24-24V160H248c-13.2 0-24-10.8-24-24zm64 236c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-64c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12v8zm0-72v8c0 6.6-5.4 12-12 12H108c-6.6 0-12-5.4-12-12v-8c0-6.6 5.4-12 12-12h168c6.6 0 12 5.4 12 12zm96-114.1v6.1H256V0h6.1c6.4 0 12.5 2.5 17 7l97.9 98c4.5 4.5 7 10.6 7 16.9z" class=""></path></svg></a>
            </div>
          </div>
        </div>
        {{-- @php(dd($data)) --}}
        <div class="card-body">
          <div class="search-bar">
            <div class="input-group input-group-alternative">
              <div class="search_form-group w-100">
                {{--  <div class="form-group">
                  <label for="" class="form-control-label text-white">Search Database:</label>
                </div> --}}
                
                <div class="dropdown w-100">
                  <button type="button" id="menu-toggle" class="btn btn-primary btn-sm float-right dropdown-toggle  " data-toggle="dropdown" aria-expanded="false"><span class="caret"></span>Search Filters</button>
                  <div class="dropdown-menu w-100 " id="search_wrapper" role="menu">
                    <form class="form-horizontal" role="form">
                      <div class="form-row">
                        <div class="col-md-4">
                          <div class="form-group  ">
                            <label for="filter" class="form-control-label">Search by Name</label>
                            <div class="input-group input-group-alternative">
                              <input class="form-control searchFilter" type="text" name="search_name" id="search_name" placeholder="type employee name" data-order="3" />
                              
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group  ">
                            <label for="filter" class="form-control-label">Search by Employee ID</label>
                            <div class="input-group input-group-alternative">
                              <input class="form-control searchFilter" type="text" name="search_empid" id="search_empid" placeholder="type employee id" data-order="2" />
                              
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4">
                          <div class="form-group  ">
                            <label for="filter" class="form-control-label">Search by BPS</label>
                            <div class="input-group input-group-alternative">
                              
                              <input class="form-control searchFilter" type="text" name="search_bps" id="search_bps" placeholder="type employee bps" data-order="4"/>
                            </div>
                            
                          </div>
                        </div>
                      </div>
                      <div class="form-row  ">
                        
                        <div class="col-md-4">
                          <div class="form-group  ">
                            <label for="filter" class="form-control-label">Search by Rank</label>
                            <div class="input-group input-group-alternative">
                              <input class="form-control searchFilter" type="text" name="search_rank" id="search_rank" placeholder="type employee rank" data-order="5"/>
                              
                            </div>
                          </div>
                        </div>
                        <div class="col-md-8">
                          <div class="form-group  ">
                            <label for="filter" class="form-control-label">Search by Date of Enrolment</label>
                            <div class="row ">
                              
                              <div class="col-md-6 pl-0">
                                <div class="input-group input-group-alternative">
                                  
                                  <input class="form-control  datetimepicker" type="text" name="search_date_of_enrol_FROM" id="search_date_of_enrol_FROM" placeholder="from" data-order="6"/>
                                </div>
                              </div>
                              <div class="col-md-6 pr-0">
                                <div class="input-group input-group-alternative">
                                  
                                  <input class="form-control  datetimepicker" type="text" name="search_date_of_enrol_TO" id="search_date_of_enrol_TO" placeholder="to" data-order="6"/>
                                </div>
                              </div>
                            </div>
                            
                            
                          </div>
                        </div>
                      </div>
                      <div class="form-row  ">
                        
                        
                        <div class="col-md-8">
                          <div class="form-group  ">
                            <label for="filter" class="form-control-label">Search by Age</label>
                            <div class="row ">
                              
                              <div class="col-md-6 pl-0">
                                <div class="input-group input-group-alternative">
                                  
                                  <input class="form-control" type="text" name="search_by_age_from" id="search_by_age_from" placeholder="from" data-order="6"/>
                                </div>
                              </div>
                              <div class="col-md-6 pr-0">
                                <div class="input-group input-group-alternative">
                                  
                                  <input class="form-control" type="text" name="search_by_age_to" id="search_by_age_to" placeholder="to" data-order="6"/>
                                </div>
                              </div>
                            </div>
                            
                            
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                
              </div>   </div>
              
              
            </div>
            <div class="table-responsive" id="wrapper">
              <table class="table align-items-center table-flush" id="myTable">
                <thead class="thead-light">
                  <tr>
                    <th scope="col" class="sort" data-sort="0"></th>
                    <th scope="col" class="sort" data-sort="1">Image</th>
                    <th scope="col" class="sort" data-sort="2">Employee ID</th>
                    <th scope="col" class="sort" data-sort="3">Name</th>
                    <th scope="col" class="sort" data-sort="4">Age</th>
                    <th scope="col" class="sort" data-sort="5">BPS</th>
                    <th scope="col" class="sort" data-sort="6">Rank</th>
                    <th scope="col" class="sort" data-sort="7">Date of Enrolment</th>
                    <th scope="col" class="sort" data-sort="8">Present Unit</th>
                    <th scope="col" class="sort" data-sort="9">Date of Present Rank</th>
                    <th scope="col" class="sort" data-sort="10">Action</th>
                  </tr>
                </thead>
                <tbody class="list">
                  @php($i = 1)
                  @foreach($data as $item)
                  {{-- @php(dd(gettype($item->date_of_birth))) --}}
                  <tr>
                    
                    <td>{{ $i++}}</td>
                    <td><div class="profile-image"><a onclick="open_modal('{{$item->image}}')" data-toggle="modal" data-target="#exampleModal"><img class="" src="{{ !is_null($item->image) ? asset('/images/'.$item->image) : asset('/images/employee-avatar.png') }}" alt=""></a></div></td>
                    <td>{{ $item->employee_id}}</td>
                    <td>{{ $item->name}}</td>
                     <td>{{(int)Carbon\Carbon::now()->toDateTimeString() - (int)$item->date_of_birth}}</td>
                    <td>{{ $item->bps}}</td>
                    <td>{{ $item->rank}}</td>
                    <td>{{ $item->date_of_enrolment}}</td>
                    <td>{{ $item->present_unit->name}}</td>
                    <td>{{ $item->date_of_present_rank}}</td>
                    <td class="text-right">
                      <div class="dropdown dropleft">
                        <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fas fa-ellipsis-v"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <a class="dropdown-item" href="{{ route('employee.view', ['employee_id' => $item->employee_id]) }}">View Complete Info</a>
                          <a class="dropdown-item" href="{{ route('employee.edit', ['employee_id' => $item->employee_id]) }}">Edit Employee Record</a>
                          <a class="dropdown-item" href="{{ route('employement.edit', ['employee_id' => $item->employee_id]) }}">Edit Employement Record</a>
                          <a class="dropdown-item" href="{{ route('techqual.edit', ['employee_id' => $item->employee_id]) }}">Edit Technical Qualification</a>
                          <a class="dropdown-item" href="{{ route('civqual.edit', ['employee_id' => $item->employee_id]) }}">Edit Civil Qualification</a>
                          <a class="dropdown-item" href="{{ route('punishrec.edit', ['employee_id' => $item->employee_id]) }}">Edit Punishment Record</a>
                        </div>
                      </div>
                    </td>
                    {{-- <td hidden>{{$item->date_of_birth}}</td> --}}
                   
                  </tr>
                  @endforeach
                  
                  
                </tbody>
              </table>
              <div class="modal" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header p-0 m-0">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body profile-img-modal">
                      <img class="" src="" alt="">
                      {{-- <p>Modal body text goes here.</p> --}}
                    </div>
                    {{--<div class="modal-footer">
                      <button type="button" class="btn btn-primary">Save changes</button>
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div> --}}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="overLay d-none "><div class="loader"><img class="img-fluid" src="{{asset('images/loader.gif')}}" alt=""></div></div>
      {{-- <a href="" id="zipDownload"  download> DONLOAD</a>
      --}}
      <script src="./vendor/jquery/dist/jquery.min.js"></script>
      <script>
$(document).on("click", function() {
  setTimeout(function() {
    if($(".search_form-group div.dropdown").hasClass("show")) {} else {
      $("div#wrapper.table-responsive").removeClass("active");
    }
  }, 10);
});
$("#menu-toggle").click(function(e) {
  e.preventDefault();
  $("#wrapper").toggleClass("active");
});

function open_modal(image) {
  console.log(image);
  // event.preventDefault();
  if(image != '') {
    $('.modal').modal('show');
    $('.modal-body img').attr('src', 'images/' + image);
  }
  // $('.modal-body img').src = image;
}
$(document).ready(function() {
  // fill_datatable();
  var dtable = $('#myTable').DataTable({
    sDom: 'lrtip',
    "bSort": false,
    "pagingType": "full_numbers",
    "scrollX": true,
    "retrieve": true,
    "lengthMenu": [
      [25, 50, 100, 250, -1],
      [25, 50, 100, 250, "All"]
    ],
    "autoWidth": false,
    "oLanguage": {
      "sEmptyTable": "No data available in table",
      "sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
      "sInfoEmpty": "Showing 0 to 0 of 0 entries",
      "sInfoFiltered": "(filtered from _MAX_ total entries)",
      "sLengthMenu": "Select Number of Entries _MENU_",
      "sLoadingRecords": "Loading...",
      "sProcessing": "Processing...",
      "sZeroRecords": "No matching records found"
    }
  });
  var today = new Date('Y-m-d');
  $('.datetimepicker').datetimepicker({
    timepicker: false,
    format: 'Y-m-d'
      // maxDateTime: today
  });
  $('.searchFilter').on('keyup', function() {
    if(this.value.length) {
      dtable.column($(this).attr('data-order')).search(this.value, true);
      dtable.draw();
    }
    if(!$(this).val()) {
      dtable.column($(this).attr('data-order')).search(this.value, true);
      dtable.draw();
    }
  });
  $('#search_date_of_enrol_FROM, #search_date_of_enrol_TO').on('change', function() {
    if($('#search_date_of_enrol_FROM').val() != '' && $('#search_date_of_enrol_TO').val() != '') {
      $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
        var from = $('#search_date_of_enrol_FROM').val();
        var to = $('#search_date_of_enrol_TO').val();
        var column = data[7];
        if(from <= column && column <= to) {
          return true;
        }
        return false;
      });
      dtable.draw();
      $.fn.dataTable.ext.search.pop();
    } else {
      dtable.draw();
    }
  });

    $('#search_by_age_from, #search_by_age_to').on('keyup', function() {
    if($('#search_by_age_from').val() != '' && $('#search_by_age_to').val() != '') {
      $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
        var from = $('#search_by_age_from').val();
        var to = $('#search_by_age_to').val();
        var column = data[4];
        if(from <= column && column <= to) {
          return true;
        }
        return false;
      });
      dtable.draw();
      $.fn.dataTable.ext.search.pop();
    } else {
      dtable.draw();
    }
  });


  $("#download_all").on('click', function(event) {
    event.preventDefault();
    var current_ids = [];
    var confirmation = confirm("Are you sure you want to download?");
    var current = dtable.rows({
      page: 'current'
    }).data().each(function(index, el) {
      current_ids.push(index[2])
    });
    if(confirmation) {
      $('.overLay').removeClass('d-none');
      $.ajax({
        url: '{{ route('download_pdf_all')}}',
        type: 'get',
        data: {
          _token: '{{csrf_token()}}',
          IDs: current_ids
        },
        success: function(data) {
          window.location = data;
          $('.overLay').addClass('d-none');
        },
        error: function(data) {
          alert('No Records Found!');
          // $('.overLay').addClass('d-none');
          location.reload();
        }
      });
    } else {}
  }); 



   $("#download_merged").on('click', function(event) {
    event.preventDefault();
    var current_ids = [];
    var confirmation = confirm("Are you sure you want to download?");
    var current = dtable.rows({
      page: 'current'
    }).data().each(function(index, el) {
      current_ids.push(index[2])
    });
    if(confirmation) {
      $('.overLay').removeClass('d-none');
      $.ajax({
        url: '{{ route('pdf_merged')}}',
        type: 'get',
        data: {
          _token: '{{csrf_token()}}',
          IDs: current_ids
        },
        success: function(data) {
          window.location = data;
          $('.overLay').addClass('d-none');
        },
        error: function(data) {
          alert('No Records Found!');
          // $('.overLay').addClass('d-none');
          location.reload();
        }
      });
    } else {}
  });
});
      </script>
      @endsection