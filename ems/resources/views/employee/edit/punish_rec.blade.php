@extends('layouts.app')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.datetimepicker.css') }}">

<div class="container">
   <div class="row">
        <div class="col-xl-12 order-xl-1">
          <div class="card bg-form">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Update Punishment Record</h3>
                </div>
                <div class="col-4 text-right">
                  <a href="{{url()->previous()}}" title="Go To Previous Page" class="btn btn-sm btn-primary">go back</a>
                  <a href="{{ route('punishrec.add', ['employee_id' => $data[0]->employee_id]) }}" title="Add More Records" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i></a>

                </div>
              </div>
            </div>
            {{-- @php(dd($data)) --}}
            <div class="card-body">
                  <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}"><strong>{{ Session::get('alert-' . $msg) }}</strong> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                        @endforeach
                      </div>
              <form id="employeeForm" action="{{route('punishrec.update')}}" method="post">
                  {{ csrf_field() }}

                <!-- Address -->
                {{-- <h6 class="heading-small text-secondary mb-4"></h6> --}}
                @php($i=1)
                  @foreach($data as $item)
                 <div class="row">
                   <div class="col-md-6">
                      <h3 class="text-white" style="  text-decoration: underline;">Record # {{ $i++}}</h3>

                   </div>
                   <div class="col-md-6">
                     <a class="btn btn-danger btn-sm float-right" onclick="remove_record('{{$item->nature_of_offence}}','{{$item->employee_id}}')"><i class="fa fa-trash"></i></a>
                   </div>
                 </div>
                  <input type="text" hidden name="employee_id" id="employee_id" value="{{$item->employee_id}}">

                    {{-- <h6 class="heading-small text-secondary mb-4">Punishment Record</h6> --}}
                  <div class="row">
                     <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="punishment_name">Punishment Awarded</label>
                        <input type="text" id="punishment_name" name="punishment_name[]" class="form-control" placeholder="N/A " value="{{$item->punishment_awarded}}" >
                        {{-- <div id="t_institue_inputs"></div> --}}
                      </div>
                    </div>
                    </div>
                 <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="nature_offence">Nature Of Offence</label>
                        <input id="nature_offence" name="nature_offence[]" class="form-control" placeholder="N/A" value="{{$item->nature_of_offence}}"  type="text">
                        {{-- <div id="t_course_name_inputs"></div> --}}
                      </div>
                    </div>
                      <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="punished_by">Punished By</label>
                           <select class="form-control" id="punished_by" name="punished_by[]">
                           @if (isset($item->punished_by))
                                <option value="{{ $item->punished_by }}" selected hidden>{{ $item->punished_by }}</option>
                                  <option value="A">A</option>
                                   <option value="B">B</option>
                          @else
                                 <option value="" selected hidden>Select</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                          @endif
                           </select>
                        {{-- <input id="utilization_of_posting" name="utilization_of_posting" class="form-control datetimepicker" placeholder="2020-04-01" type="text"> --}}
                      </div>
                    </div>
                  </div>
                    @endforeach
                        <div class="row">
                    <div class="col-md-12">
                      <input type="submit" value="Update" class="float-right btn btn-sm btn-success">
                    </div>
                 
                    </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery.datetimepicker.full.js') }}"></script>
             
<script>
function remove_record(nature_of_offence, employee_id){
  // console.log(a, b);
var confirmation = confirm("Do you really want to remove this record ?");

if(confirmation){
    $.ajax({
    url:' {{ route('punishrec.remove')}}',
    type: 'GET',
    data: {
      nature_of_offence, employee_id
    },
  })
  .done(function() {
      // alert('Record Removed!');
       location.reload();
  })
  .fail(function() {
      alert('Something is wrong!');
  });
}
  
}

</script>
@endsection