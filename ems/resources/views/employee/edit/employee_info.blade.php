@extends('layouts.app')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.datetimepicker.css') }}">

<div class="container">
   <div class="row">
        <div class="col-xl-12 order-xl-1">
          <div class="card bg-form">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Update Employee Info</h3>
                </div>
                <div class="col-4 text-right">
                  <a href="{{url()->previous()}} " title="Go To Previous Page" class="btn btn-sm btn-primary">go back</a>
                </div>
              </div>
            </div>
            {{-- @php(dd($data)) --}}
            <div class="card-body">
                  <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}"><strong>{{ Session::get('alert-' . $msg) }}</strong> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                        @endforeach
                      </div>
              <form id="employeeForm" action="{{route('employee.update')}}" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}

                {{-- <h6 class="heading-small text-secondary mb-4">Personal information</h6> --}}
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-3">
                      <div class="form-group">
                        <div class="card" >
                          <div  class=" card-img-parent" >

  <img id="card-img-top" class="card-img-top img-thumbnail" src="{{ !is_null($data->image) ? asset('/images/'.$data->image) : asset('/images/employee-avi.jpg') }}" alt="Card image cap">
  <div class="card-body">
  {{--   <h5 class="card-title">Card title</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> --}}
  </div>
  <div class="file-wrapper">
    
<label >
       Choose Image
    </label>

    {{-- <input id="image" type="file"/> --}}
    <input type="text" name="file_update" id="file_update" hidden value="{{ !is_null($data->image) ? $data->image : null }}">
    <input style="display: none;" accept=".jpg,.jpeg,.png" type="file" id="image" name="image" class="btn btn-primary">
  </div>
  </div>
</div>
{{--  <label class="form-control-label text-white" for="rank">Image</label>
                        <div class="card">
  <div class="card-body">
  </div>
                        <input type="file" id="rank" name="rank" class="form-control" placeholder="PRO" >
</div>
                    --}}
                      </div>
                    </div> 

                    <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="employee_id">Employee ID</label>
                        <input type="text" id="employee_id" name="employee_id" class="form-control" placeholder="N/A" value="{{$data->employee_id}}" >
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="rank">Rank</label>
                        <input type="text" id="rank" name="rank" class="form-control" placeholder="N/A" value="{{$data->rank}}" >
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="bps">BPS</label>
                        <input type="text" id="bps" name="bps" class="form-control" placeholder="N/A" value="{{$data->bps}}">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="name">Full Name</label>
                        <input type="text" id="name" name="name" class="form-control" placeholder="N/A" value="{{$data->name}}">
                      </div>
                    </div>
                    </div>
                  <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="date_of_birth">Date of Birth</label>
                        <input type="text" id="date_of_birth" name="date_of_birth" value="{{$data->date_of_birth}}" class="datetimepicker form-control" placeholder="N/A" >
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="cnic">CNIC #</label>
                        <input type="text" id="cnic" name="cnic" class="form-control" value="{{$data->cnic}}" placeholder="N/A">
                      </div>
                    </div>
                    </div>
                  <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="domicile">Domicile</label>
                        <input type="text" id="domicile" name="domicile" value="{{$data->domicile}}" class="form-control" placeholder="N/A">
                      </div>
                    </div>
                      <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white"  for="blood_group">Blood Group</label>
                        <select class="form-control" name="blood_group"  id="blood_group">
                          <option value="{{$data->blood_group}}" selected >{{$data->blood_group}}</option>
                          <option value="A+" >A+</option>
                          <option value="A-" >A-</option>
                          <option value="B+" >B+</option>
                          <option value="B-" >B-</option>
                          <option value="O+" >O+</option>
                          <option value="O-" >O-</option>
                          <option value="AB+" >AB+</option>
                          <option value="AB-" >AB-</option>
                        </select>
                      </div>
                     {{--  <div class="form-group">
                        <label class="form-control-label text-white" for="input-address">Blood Group</label>
                        <input id="input-address" class="form-control" placeholder="Home Address"  type="text">
                      </div> --}}
                    </div>
                  </div>
                  </div>
                  </div>
                <hr class="my-4" />
                <!-- Address -->
                <h6 class="heading-small text-secondary mb-4">Contact information</h6>
                <div class="pl-lg-4">
                  <div class="row">
                  
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="email">Email Address</label>
                        <input type="email" id="email" name="email" class="form-control" value="{{$data->email}}" placeholder="N/A">
                      </div>
                    </div>
                  {{-- </div> --}}
                  {{-- <div class="row"> --}}
                    <div class="col-lg-4">
                       <div class="form-group">
    <label class="form-control-label text-white" for="marital_status">Marital Status</label>
    <select class="form-control" id="marital_status" name="marital_status">
      <option value="{{$data->marital_status}}" selected >{{$data->marital_status}}</option>
      <option value="single" >single</option>
      <option value="married" >married</option>
      <option value="widowed" >widowed</option>
      <option value="divorced" >divorced</option>
      <option value="separated" >separated</option>
    </select>
  </div>
                     {{--  <div class="form-group">
                        <label class="form-control-label text-white" for="input-city">Marital Status</label>
                        <input type="text" id="input-city" class="form-control" placeholder="City" value="New York">
                      </div> --}}
                    </div>
                    <div class="col-lg-4">
                                          <div class="form-group">
    <label class="form-control-label text-white"  for="no_of_children">No. of Children</label>
    <select class="form-control" id="no_of_children" name="no_of_children">
      <option value="{{$data->no_of_children}}" selected >{{$data->no_of_children}}</option>
      <option value="0" >0</option>
      <option value="1" >1</option>
      <option value="2" >2</option>
      <option value="3" >3</option>
      <option value="4" >4</option>
      <option value="5" >5</option>
      <option value="6" >6</option>
      <option value="7" >7</option>
      <option value="8" >8</option>
    </select>
  </div>
                   {{--    <div class="form-group">
                        <label class="form-control-label text-white" for="no_of_children">No. of Children</label>
                        <input type="text" id="no_of_children" name="no_of_children" class="form-control" placeholder="Country" value="United States">
                      </div> --}}
                    </div>
                    </div>
                  <div class="row">

                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="present_address">Present Address</label>
                        <input type="text" id="present_address" name="present_address" value="{{$data->present_address}}"  class="form-control" placeholder="N/A">
                      </div>
                    </div>
                  </div> 

                  <div class="row">

                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="permanent_address">Permanent Address</label>
                        <input type="text" id="permanent_address" name="permanent_address" value="{{$data->permanent_address}}"  class="form-control" placeholder="N/A">
                      </div>
                    </div>
                  </div>
                      <hr class="my-4" />
           
                <h6 class="heading-small text-secondary mb-4">Employement information</h6>
                    <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="date_of_enrolment">Date of Enrolment</label>
                        <input id="date_of_enrolment" name="date_of_enrolment" value="{{$data->date_of_enrolment}}" class="datetimepicker form-control" placeholder="N/A"  type="text">
                      </div>
                    </div>
                  {{-- </div> --}}
                  {{-- <div class="row"> --}}

                    {{-- @php(dd($data->present_unit->name)) --}}
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="present_unit_id">Present Unit</label>
                            <select class="form-control" id="present_unit_id" name="present_unit_id">
                            <option value="{{$data->present_unit->id}}" selected hidden>{{$data->present_unit->name}}</option>
                             @foreach($units as $unit)
                              <option value="{{$unit->id}}" >{{$unit->name}}</option>
                             @endforeach
                           </select>
                        {{-- <input type="text" id="present_unit_id" name="present_unit_id" class="form-control" placeholder="ASID"> --}}
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="date_of_present_rank">Date of Present Rank</label>
                        <input type="text" id="date_of_present_rank" name="date_of_present_rank" value="{{$data->date_of_present_rank}}" class="form-control datetimepicker" placeholder="N/A">
                      </div>
                    </div>
                    </div>

                      <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="utilization_of_posting">Utilization of Posting</label>
                           <select class="form-control" id="utilization_of_posting" name="utilization_of_posting">
                            <option value="{{$data->utilization_of_posting}}" hidden selected >{{$data->utilization_of_posting}}</option>
                            <option value="Not Avail">Not Avail</option>
                            <option value="Avail">Avail</option>
                           </select>
                        {{-- <input id="utilization_of_posting" name="utilization_of_posting" class="form-control datetimepicker" placeholder="2020-04-01" type="text"> --}}
                      </div>
                    </div>
                  {{-- </div> --}}
                  {{-- <div class="row"> --}}
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="completion_of_25yrs">Completion of 25 Years</label>
                        <input type="text" id="completion_of_25yrs" value="{{$data->completion_of_25yrs}}" name="completion_of_25yrs" class="datetimepicker form-control" placeholder="N/A">
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="superannuation">Superannuation</label>
                        <input type="text" id="superannuation" name="superannuation" value="{{$data->superannuation}}" class="datetimepicker form-control" placeholder="N/A">
                      </div>
                    </div>
                    </div>
    <hr class="my-4" />
                <!-- Address -->
                <h6 class="heading-small text-secondary mb-4">Qualification at time of Enrolment</h6>
                           <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="qual_at_time_enrol">Qualification at time of Enrolment</label>
                        <input id="qual_at_time_enrol" name="qual_at_time_enrol" value="{{$data->qual_at_time_enrol}}" class="form-control" placeholder="N/A"  type="text">
                      </div>
                    </div>
                  {{-- </div> --}}
                  {{-- <div class="row"> --}}
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="division">Division</label>
                        <input type="text" id="division" name="division" value="{{$data->division}}" class="form-control" placeholder="N/A" >
                      </div>
                    </div>
                    </div>                
              
                            <div class="row">
                    <div class="col-md-12">
                      <input type="submit" value="Update" class="float-right btn btn-sm btn-success">
                    </div>
                 
                    </div>
                    </div>
                </div>
              
              </form>
            </div>
          </div>
        </div>
      </div>
      </div>

  <script src="./vendor/jquery/dist/jquery.min.js"></script>

<script>
$(document).ready(function() {
  var today = new Date('Y-m-d');
    $('.datetimepicker').datetimepicker({
    timepicker:false,
    format:'Y-m-d'
    // maxDateTime: today

  });  

});

var inp =  document.getElementById('image');
inp.addEventListener('change', function(e){
    var file = this.files[0];
    var reader = new FileReader();
    reader.onload = function(){
        document.getElementById('card-img-top').src = this.result;
        };
    reader.readAsDataURL(file);
    },false);

$(".file-wrapper").click(function () {
    $("#image").trigger('click');
});




</script>
@endsection