@extends('layouts.app')
@section('content')

{{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.datetimepicker.css') }}"> --}}
<div class="container">
  <div class="row">
    <div class="col-xl-12 order-xl-1">
      <div class="card bg-form">
        <div class="card-header">
          <div class="row align-items-center">
            <div class="col-8">
              <h3 class="mb-0">Employee Profile - ID {{$data->employee_id}}</h3>
            </div>
               <div class="col-4 text-right">
                  <a href="{{url()->previous()}}" title="Go To Previous Page" class="btn btn-sm btn-primary">go back</a>
                
                  <a href="{{route('downloadpdf',['employee_id' => $data->employee_id])}}" class="btn btn-sm btn-danger" title="Download as PDF"><svg width="15px" height="15px" aria-hidden="true" focusable="false" data-prefix="far" data-icon="file-pdf" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" class="svg-inline--fa fa-file-pdf fa-w-12 fa-2x"><path fill="currentColor" d="M369.9 97.9L286 14C277 5 264.8-.1 252.1-.1H48C21.5 0 0 21.5 0 48v416c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48V131.9c0-12.7-5.1-25-14.1-34zM332.1 128H256V51.9l76.1 76.1zM48 464V48h160v104c0 13.3 10.7 24 24 24h104v288H48zm250.2-143.7c-12.2-12-47-8.7-64.4-6.5-17.2-10.5-28.7-25-36.8-46.3 3.9-16.1 10.1-40.6 5.4-56-4.2-26.2-37.8-23.6-42.6-5.9-4.4 16.1-.4 38.5 7 67.1-10 23.9-24.9 56-35.4 74.4-20 10.3-47 26.2-51 46.2-3.3 15.8 26 55.2 76.1-31.2 22.4-7.4 46.8-16.5 68.4-20.1 18.9 10.2 41 17 55.8 17 25.5 0 28-28.2 17.5-38.7zm-198.1 77.8c5.1-13.7 24.5-29.5 30.4-35-19 30.3-30.4 35.7-30.4 35zm81.6-190.6c7.4 0 6.7 32.1 1.8 40.8-4.4-13.9-4.3-40.8-1.8-40.8zm-24.4 136.6c9.7-16.9 18-37 24.7-54.7 8.3 15.1 18.9 27.2 30.1 35.5-20.8 4.3-38.9 13.1-54.8 19.2zm131.6-5s-5 6-37.3-7.8c35.1-2.6 40.9 5.4 37.3 7.8z" class=""></path></svg></a>
               
               <div class="dropdown dropleft">
                        <a class="btn btn-sm btn-icon-only text-dark" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" title="Edit Records" aria-expanded="false">
                        <i class="fa fa-edit"></i>                 
                               </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <a class="dropdown-item" href="{{ route('employee.edit', ['employee_id' => $data->employee_id]) }}">Edit Employee Record</a>
                          <a class="dropdown-item" href="{{ route('employement.edit', ['employee_id' => $data->employee_id]) }}">Edit Employement Record</a>
                          <a class="dropdown-item" href="{{ route('techqual.edit', ['employee_id' => $data->employee_id]) }}">Edit Technical Qualification</a>
                          <a class="dropdown-item" href="{{ route('civqual.edit', ['employee_id' => $data->employee_id]) }}">Edit Civil Qualification</a>
                          <a class="dropdown-item" href="{{ route('punishrec.edit', ['employee_id' => $data->employee_id]) }}">Edit Punishment Record</a>
                        </div>
                      </div>
                     <div class="dropdown dropleft">
                        <a class="btn btn-sm btn-icon-only text-dark" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" title="Add More Records" aria-expanded="false">
                          <i class="fa fa-plus-circle"></i>                       
                           </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                          <a class="dropdown-item" href="{{ route('empRec.add', ['employee_id' => $data->employee_id]) }}">Add Employement Record</a>
                          <a class="dropdown-item" href="{{ route('techQual.add', ['employee_id' => $data->employee_id]) }}">Add Technical Qualification</a>
                          <a class="dropdown-item" href="{{ route('civQual.add', ['employee_id' => $data->employee_id]) }}">Add Civil Qualification</a>
                          <a class="dropdown-item" href="{{ route('punishrec.add', ['employee_id' => $data->employee_id]) }}">Add Punishment Record</a>
                        </div>
                      </div>
            </div>
          </div>
        </div>
        {{-- @php(dd($data)) --}}
        <div class="card-body">
          {{-- <h6 class="heading-small text-secondary mb-4">Employement Record</h6> --}}
          <h3 class="heading-small text-secondary mb-4">PERSONAL INFORMATION</h3>
          <div class="row">
            <div class="col-lg-3">
              <div class="form-group">
                <div class="card" >
                  <div  class=" card-img-parent" >
                  <img id="card-img-top" class="card-img-top card-img-inner img-thumbnail" src="{{ !is_null($data->image) ? asset('/images/'.$data->image) : asset('/images/employee-avi.jpg') }}" alt="Card image cap">
                </div>
                </div>
              </div>
              
            </div>
            <div class="col-lg-9">
              <div class="row">
                <div class="col-lg-12">
                  <div class="row">
                    <div class="col-lg-3">
                      <h4 class="text-white mb-3">Employee ID:</h4>
                    </div>
                    <div class="col-lg-3">
                      <span class="text-white">{{$data->employee_id ?? 'N/A'}}</span>
                      
                    </div>
                    <div class="vertical-bar"></div>
                    <div class="col-lg-3">
                      <h4 class="text-white mb-3">Rank:</h4>
                    </div>
                    <div class="col-lg-3">
                      <span class="text-white">{{$data->rank ?? 'N/A'}}</span>
                      
                    </div>
                  </div>
                  
                </div>
              </div>
               <div class="row">
                <div class="col-lg-12">
                  <div class="row">
                    <div class="col-lg-3">
                      <h4 class="text-white mb-3">BPS:</h4>
                    </div>
                    <div class="col-lg-3">
                      <span class="text-white">{{$data->bps ?? 'N/A'}}</span>
                      
                    </div>
                    <div class="vertical-bar"></div>
                    <div class="col-lg-3">
                      <h4 class="text-white mb-3">CNIC#:</h4>
                    </div>
                    <div class="col-lg-3">
                      <span class="text-white">{{$data->cnic ?? 'N/A'}}</span>
                      
                    </div>
                  </div>
                  
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="row">
                    <div class="col-lg-3">
                      <h4 class="text-white mb-3">Full Name:</h4>
                    </div>
                    <div class="col-lg-3">
                      <span class="text-white">{{$data->name ?? 'N/A'}}</span>
                      
                    </div>
                    <div class="vertical-bar"></div>
                    <div class="col-lg-3">
                      <h4 class="text-white mb-3">Date of Birth:</h4>
                    </div>
                    <div class="col-lg-3">
                      <span class="text-white">{{$data->date_of_birth ?? 'N/A'}}</span>
                      
                    </div>
                  </div>
                  
                </div>
              </div>
               <div class="row">
                <div class="col-lg-12">
                  <div class="row">
                    <div class="col-lg-3">
                      <h4 class="text-white mb-3">Blood Group:</h4>
                    </div>
                    <div class="col-lg-3">
                      <span class="text-white">{{$data->blood_group ?? 'N/A'}}</span>
                      
                    </div>
                    <div class="vertical-bar"></div>
                    <div class="col-lg-3">
                      <h4 class="text-white mb-3">Domicile:</h4>
                    </div>
                    <div class="col-lg-3">
                      <span class="text-white">{{$data->domicile ?? 'N/A'}}</span>
                      
                    </div>
                  </div>
                  
                </div>
              </div>
               <div class="row">
                <div class="col-lg-12">
                  <div class="row">
                    
                    <div class="vertical-bar"></div>
                    <div class="col-lg-3">
                      <h4 class="text-white mb-3">Marital Status:</h4>
                    </div>
                    <div class="col-lg-3">
                      <span class="text-white">{{$data->marital_status ?? 'N/A'}}</span>
                      
                    </div>
                    <div class="col-lg-3">
                      <h4 class="text-white mb-3">No. of Children:</h4>
                    </div>
                    <div class="col-lg-3">
                      <span class="text-white">{{$data->no_of_children ?? 'N/A'}}</span>
                      
                    </div>
                  </div>
                  
                </div>
              </div>
               <div class="row">
                <div class="col-lg-12">
                  <div class="row">
                    <div class="col-lg-3">
                      <h4 class="text-white mb-3">Email Address:</h4>
                    </div>
                    <div class="col-lg-9">
                      <span class="text-white">{{$data->email ?? 'N/A'}}</span>
                      
                    </div>
                    <div class="col-lg-3">
                      <h4 class="text-white mb-3">Present Address:</h4>
                    </div>
                    <div class="col-lg-9">
                      <span class="text-white">{{$data->present_address ?? 'N/A'}}</span>
                      
                    </div>
                    <div class="col-lg-3">
                      <h4 class="text-white mb-3">Permanent Address:</h4>
                    </div>
                    <div class="col-lg-9">
                      <span class="text-white">{{$data->permanent_address ?? 'N/A'}}</span>
                      
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          </div>
 <hr class="my-4" />

          <div class="row">
            <div class="col-lg-12">
          <h3 class="heading-small text-secondary mb-4">EMPLOYEMENT INFORMATION</h3>
                 <div class="table-responsive">

            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col" class="sort" data-sort="unit">#</th>
                    <th scope="col" class="sort" data-sort="unit">Date of Enrolment</th>
                    <th scope="col" class="sort" data-sort="from">Present Unit</th>
                    <th scope="col" class="sort" data-sort="from">Date of Present Rank</th>
                    <th scope="col" class="sort" data-sort="from">Utilization of Posting</th>
                    <th scope="col" class="sort" data-sort="from">Completion of 25 Years</th>
                    <th scope="col" class="sort" data-sort="from">Superannuation</th>
                   
                  </tr>
                </thead>
                <tbody class="list">
                 <tr>
                   <td></td>
                   <td>{{ $data->date_of_enrolment ?? 'N/A'}}</td>
                   <td>{{ $data->present_unit->name ?? 'N/A'}}</td>
                   <td>{{ \Carbon\Carbon::parse($data->date_of_present_rank)->format('Y-m-d') ?? 'N/A' }}</td>
                   <td>{{ $data->utilization_of_posting ?? 'N/A'}}</td>
                   <td>{{ $data->completion_of_25yrs ?? 'N/A'}}</td>
                   <td>{{ $data->superannuation ?? 'N/A'}}</td>
                  
                 </tr>
              
               
                </tbody>
              </table>
              
            </div>
            </div>
          </div>

            <hr class="my-4" />

          <div class="row">
            <div class="col-lg-12">
          <h3 class="heading-small text-secondary mb-4">QUALIFICATION AT TIME OF ENROLMENT</h3>
                 <div class="table-responsive">

            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col" class="sort" data-sort="unit">Course Name</th>
                    <th scope="col" class="sort" data-sort="from">Division</th>
                   
                  </tr>
                </thead>
                <tbody class="list">
                  {{-- @foreach($data as $item) --}}
                  <tr>
                    <td>{{ $data->qual_at_time_enrol ?? 'N/A' }}</td>
                    <td>{{ $data->division ?? 'N/A' }}</td>
                   
              
                  </tr>
                {{-- @endforeach --}}
              
               
                </tbody>
              </table>
              
            </div>
            </div>
          </div>


                <hr class="my-4" />

          <div class="row">
            <div class="col-lg-12">
          <h3 class="heading-small text-secondary mb-4">EMPLOYEMENT RECORD</h3>
                 <div class="table-responsive">

            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col" class="sort" data-sort="unit">Unit</th>
                    <th scope="col" class="sort" data-sort="from">From</th>
                    <th scope="col" class="sort" data-sort="to">To</th>
                   
                  </tr>
                </thead>
                <tbody class="list">
                  @foreach($data->employement_record as $item)
                    {{-- @php(dd($item->units)) --}}
                      @foreach($item->units as $unit)
                    <?php
                    $from = $item->from != null ? \Carbon\Carbon::parse($item->from)->format('Y-m-d') : 'N/A' ;
                    $to = $item->to != null ? \Carbon\Carbon::parse($item->to)->format('Y-m-d') : null;
                    ?>
                      <tr>
                        <td>{{ $unit->name ?? 'N/A' }}</td>
                        <td>{{ $from }}</td>
                    <td>{{ isset($from) && $to == null ? 'till present' : $to }} </td>
                       
                       
                  
                      </tr>
                       @endforeach

                @endforeach
              
               
                </tbody>
              </table>
              
            </div>
            </div>
          </div>
           <hr class="my-4" />

          <div class="row">
            <div class="col-lg-12">
          <h3 class="heading-small text-secondary mb-4">CIVIL QUALIFICATION</h3>
                 <div class="table-responsive">

            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col" class="sort" data-sort="index">#</th>
                    <th scope="col" class="sort" data-sort="course">Course Name</th>
                    <th scope="col" class="sort" data-sort="division">Division</th>
                   
                  </tr>
                </thead>
                <tbody class="list">
                  @php($i = 1)
                  @foreach($data->civil_qualification as $item)
                    {{-- @php(dd($item->course)) --}}
                  <tr>
                    <td>{{ $i++}}</td>
                    <td>{{ $item->course ?? 'N/A'}}</td>
                   <td>{{ $item->division ?? 'N/A'}}</td>
              
                  </tr>
                @endforeach
              
               
                </tbody>
              </table>
              
            </div>
            </div>
          </div>
           <hr class="my-4" />

          <div class="row">
            <div class="col-lg-12">
          <h3 class="heading-small text-secondary mb-4">TECHNICAL QUALIFICATION</h3>
                 <div class="table-responsive">

            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col" class="sort" data-sort="index">#</th>
                    <th scope="col" class="sort" data-sort="course">Course Name</th>
                    <th scope="col" class="sort" data-sort="division">Division</th>
                    <th scope="col" class="sort" data-sort="institue">Institue</th>
                    <th scope="col" class="sort" data-sort="from">From</th>
                    <th scope="col" class="sort" data-sort="to">To</th>
                   
                  </tr>
                </thead>
                <tbody class="list">
                  @php($i = 1)
                  {{-- @php(dd($data->tech_qualification)) --}}
                  @foreach($data->tech_qualification as $item)
                    <?php
                    $from = $item->from != null ? \Carbon\Carbon::parse($item->from)->format('Y-m-d') : 'N/A' ;
                    $to = $item->to != null ? \Carbon\Carbon::parse($item->to)->format('Y-m-d') : null;
                    ?>
                    {{-- @php(dd($to)) --}}
                  <tr>
                    <td>{{ $i++}}</td>
                    <td>{{ $item->course ?? 'N/A'}}</td>
                    <td>{{ $item->division ?? 'N/A'}}</td>
                    <td>{{ $item->institute ?? 'N/A'}}</td>
                    <td>{{ $from }} </td>
                   <td>{{ isset($from) && $to == null ? 'till present' : $to }} </td>
              
                  </tr>
                @endforeach
                </tbody>
              </table>
              
            </div>
            </div>
          </div>

            <hr class="my-4" />

          <div class="row">
            <div class="col-lg-12">
          <h3 class="heading-small text-secondary mb-4">PUNISHMENT RECORD</h3>
                 <div class="table-responsive">

            <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                    <th scope="col" class="sort" data-sort="index">#</th>
                    <th scope="col" class="sort" data-sort="division">Nature Of Offence</th>
                    <th scope="col" class="sort" data-sort="course">Punishment Awarded</th>
                    <th scope="col" class="sort" data-sort="institue">Punished By</th>
                   
                  </tr>
                </thead>
                <tbody class="list">
                  @php($i = 1)
                  @foreach($data->punishment_record as $item)
                    {{-- @php(dd($item->course)) --}}
                  <tr>
                    <td>{{ $i++}}</td>
                    <td>{{ $item->nature_of_offence ?? 'N/A'}}</td>
                    <td>{{ $item->punishment_awarded ?? 'N/A'}}</td>
                    <td>{{ $item->punished_by ?? 'N/A'}}</td>
              
                  </tr>
                @endforeach
                </tbody>
              </table>
              
            </div>
            </div>
          </div>
        </div>
      </div>
      <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <script>
      (function($){
  $('.dropdown-menu a.dropdown-show').on('click', function(e) {
    if (!$(this).next().hasClass('show')) {
    $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
    }
    var $subMenu = $(this).next(".dropdown-menu");
    $subMenu.toggleClass('show');

    $(this).parents('div.dropdown.show').on('hidden.bs.dropdown', function(e) {
    $('.dropdown-submenu .show').removeClass("show");
    });

    return false;
  });
})(jQuery)
    </script>
      @endsection