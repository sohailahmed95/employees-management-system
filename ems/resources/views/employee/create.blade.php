@extends('layouts.app')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.datetimepicker.css') }}">

<div class="container">
   <div class="row">
        <div class="col-xl-12 order-xl-1">
          <div class="card bg-form text-white ">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Add New Employee</h3>
                </div>
                <div class="col-4 text-right">
                  <a href="{{url()->previous()}}" title="Go To Previous Page" class="btn btn-sm btn-primary">go back</a>
              
                </div>
              </div>
            </div>
            <div class="card-body">
               <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}"><strong>{{ Session::get('alert-' . $msg) }}</strong> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                        @endforeach
                      </div>
              <form id="employeeForm" action="{{route('employee.create')}}" method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}

                <h6 class="heading-small text-secondary mb-4">Personal information</h6>
                <div class="pl-lg-4">
                  <div class="row">
                    <div class="col-lg-3">
                      <div class="form-group">
                        <div class="card" >
                          <div  class=" card-img-parent" >
  <img id="card-img-top" class="card-img-top img-thumbnail" src="{{ asset('/images/employee-avi.jpg') }}" alt="Card image cap">
  <div class="card-body">
  {{--   <h5 class="card-title">Card title</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> --}}
  </div>
  <div class="file-wrapper">
    
<label class="m-0">
       Choose Image
    </label>

    {{-- <input id="image" type="file"/> --}}
    <input  style="display: none;" accept=".jpg,.jpeg,.png" type="file" id="image" name="image" class="btn btn-primary">
  </div>
  </div>
</div>
{{--  <label class="form-control-label text-white" for="rank">Image</label>
                        <div class="card">
  <div class="card-body">
  </div>
                        <input type="file" id="rank" name="rank" class="form-control" placeholder="PRO" >
</div>
                    --}}
                      </div>
                    </div> 

                    <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="employee_id">Employee ID <span class="text-danger">*</span></label>
                        <input autocomplete="off" type="text" required=""  id="employee_id" name="employee_id" class="form-control" placeholder="24352" >
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="rank">Rank<span class="text-danger">*</span></label>
                        <input autocomplete="off" type="text" id="rank"  required=""  name="rank" class="form-control" placeholder="PRO" >
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="bps">BPS<span class="text-danger">*</span></label>
                        <input autocomplete="off" type="text" id="bps" required="" name="bps" class="form-control" placeholder="BPS-19">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="name">Full Name<span class="text-danger">*</span></label>
                        <input autocomplete="off" type="text" id="name" required="" name="name" class="form-control" placeholder="Full Name">
                      </div>
                    </div>
                    </div>
                  <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="date_of_birth">Date of Birth</label>
                        <input autocomplete="off" type="text" id="date_of_birth" name="date_of_birth" class="datetimepicker form-control" placeholder="yyyy-mm-dd" >
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="cnic">CNIC #<span class="text-danger">*</span></label>
                        <input autocomplete="off" type="text" id="cnic" required="" name="cnic" class="form-control" placeholder="xxxxx-xxxxxxx-x">
                      </div>
                    </div>
                    </div>
                  <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="domicile">Domicile<span class="text-danger">*</span></label>
                        <input autocomplete="off" type="text" id="domicile" required="" name="domicile" class="form-control" placeholder="Sukkur">
                      </div>
                    </div>
                      <div class="col-lg-6">
                      <div class="form-group">
                        <label for="blood_group">Blood Group</label>
                        <select class="form-control" name="blood_group" id="blood_group">
                          <option value="" selected hidden>Select Blood Group</option>
                          <option value="A+" >A+</option>
                          <option value="A-" >A-</option>
                          <option value="B+" >B+</option>
                          <option value="B-" >B-</option>
                          <option value="O+" >O+</option>
                          <option value="O-" >O-</option>
                          <option value="AB+" >AB+</option>
                          <option value="AB-" >AB-</option>
                        </select>
                      </div>
                     {{--  <div class="form-group">
                        <label class="form-control-label text-white" for="input-address">Blood Group</label>
                        <input id="input-address" class="form-control" placeholder="Home Address"  autocomplete="off" type="text">
                      </div> --}}
                    </div>
                  </div>
                  </div>
                  </div>
                {{-- <hr class="my-4 white" /> --}}
                <!-- Address -->
                {{-- <h6 class="heading-small text-secondary mb-4">Contact information</h6> --}}
                <div class="pl-lg-4">
                  <div class="row">
                  
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="email">Email Address</label>
                        <input type="email" id="email" name="email" class="form-control" placeholder="abc@xyz.com">
                      </div>
                    </div>
                  {{-- </div> --}}
                  {{-- <div class="row"> --}}
                    <div class="col-lg-4">
                       <div class="form-group">
    <label for="marital_status">Marital Status<span class="text-danger">*</span></label>
    <select class="form-control" required="" id="marital_status" name="marital_status">
      <option value="" selected hidden>Select Marital Status</option>
      <option value="single" >single</option>
      <option value="married" >married</option>
      <option value="widowed" >widowed</option>
      <option value="divorced" >divorced</option>
      <option value="separated" >separated</option>
    </select>
  </div>
                     {{--  <div class="form-group">
                        <label class="form-control-label text-white" for="input-city">Marital Status</label>
                        <input autocomplete="off" type="text" id="input-city" class="form-control" placeholder="City" value="New York">
                      </div> --}}
                    </div>
                    <div class="col-lg-4">
                                          <div class="form-group">
    <label for="no_of_children">No. of Children</label>
    <select class="form-control" id="no_of_children" name="no_of_children">
      <option value="" selected hidden>Select No. of Children</option>
      <option value="0" >0</option>
      <option value="1" >1</option>
      <option value="2" >2</option>
      <option value="3" >3</option>
      <option value="4" >4</option>
      <option value="5" >5</option>
      <option value="6" >6</option>
      <option value="7" >7</option>
      <option value="8" >8</option>
    </select>
  </div>
                   {{--    <div class="form-group">
                        <label class="form-control-label text-white" for="no_of_children">No. of Children</label>
                        <input autocomplete="off" type="text" id="no_of_children" name="no_of_children" class="form-control" placeholder="Country" value="United States">
                      </div> --}}
                    </div>
                    </div>
                  <div class="row">

                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="present_address">Present Address<span class="text-danger">*</span></label>
                        <input autocomplete="off" type="text" id="present_address" required="" name="present_address" class="form-control" placeholder="HNo. 23 abc street xyz colony">
                      </div>
                    </div>
                  </div> 

                  <div class="row">

                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="permanent_address">Permanent Address<span class="text-danger">*</span></label>
                        <input autocomplete="off" type="text" id="permanent_address" required="" name="permanent_address" class="form-control" placeholder="HNo. 23 abc street xyz colony">
                      </div>
                    </div>
                  </div>
                      <hr class="my-4 white" />
           
                <h6 class="heading-small text-secondary mb-4">Employement information</h6>
                    <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="date_of_enrolment">Date of Enrolment</label>
                        <input id="date_of_enrolment" name="date_of_enrolment" class="datetimepicker form-control" placeholder="yyyy-mm-dd"  autocomplete="off" type="text">
                      </div>
                    </div>
                  {{-- </div> --}}
                  {{-- <div class="row"> --}}
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="present_unit_id">Present Unit</label>
                            <select class="form-control" id="present_unit_id" name="present_unit_id">
                            <option value="" selected hidden>Select Present Unit</option>
                             @foreach($units as $unit)
                              <option value="{{$unit->id}}" >{{$unit->name}}</option>
                             @endforeach
                           </select>
                        {{-- <input autocomplete="off" type="text" id="present_unit_id" name="present_unit_id" class="form-control" placeholder="ASID"> --}}
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="date_of_present_rank">Date of Present Rank</label>
                        <input autocomplete="off" type="text" id="date_of_present_rank" name="date_of_present_rank" class="form-control datetimepicker" placeholder="yyyy-mm-dd">
                      </div>
                    </div>
                    </div>

                      <div class="row">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="utilization_of_posting">Utilization of Posting</label>
                           <select class="form-control" id="utilization_of_posting" name="utilization_of_posting">
                            <option value="" selected hidden>Please Select</option>
                            <option value="Not Avail">Not Avail</option>
                            <option value="Avail">Avail</option>
                           </select>
                        {{-- <input id="utilization_of_posting" name="utilization_of_posting" class="form-control datetimepicker" placeholder="yyyy-mm-dd" autocomplete="off" type="text"> --}}
                      </div>
                    </div>
                  {{-- </div> --}}
                  {{-- <div class="row"> --}}
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="completion_of_25yrs">Completion of 25 Years</label>
                        <input autocomplete="off" type="text" id="completion_of_25yrs" name="completion_of_25yrs" class="datetimepicker form-control" placeholder="yyyy-mm-dd">
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="superannuation">Superannuation</label>
                        <input autocomplete="off" type="text" id="superannuation" name="superannuation" class="datetimepicker form-control" placeholder="yyyy-mm-dd">
                      </div>
                    </div>
                    </div>
    <hr class="my-4 white" />
                <!-- Address -->
                <h6 class="heading-small text-secondary mb-4">Qualification at time of Enrolment</h6>
                           <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="qual_at_time_enrol">Qualification at time of Enrolment</label>
                        <input id="qual_at_time_enrol" name="qual_at_time_enrol" class="form-control" placeholder="BSc"  autocomplete="off" type="text">
                      </div>
                    </div>
                  {{-- </div> --}}
                  {{-- <div class="row"> --}}
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="division">Division</label>
                        <input autocomplete="off" type="text" id="division" name="division" class="form-control" placeholder="A" >
                      </div>
                    </div>
                    </div>


                            <hr class="my-4 white" />
                <!-- Address -->
                <h6 class="heading-small text-secondary mb-4">Employement Record</h6>
                <div class="empRec">
                  <div class="row">
                      <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="emp_unit_id">Unit</label>
                            <select class="form-control" id="emp_unit_id" name="emp_unit_id[]">
                            <option value="" selected hidden>Select Unit</option>
                             @foreach($units as $unit)
                              <option value="{{$unit->id}}" >{{$unit->name}}</option>
                             @endforeach
                           </select>
                        {{-- <input autocomplete="off" type="text" id="present_unit_id" name="present_unit_id" class="form-control" placeholder="ASID"> --}}
                      </div>
                    </div>
                     <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="emp_unitFromDate">From</label>
                        <input autocomplete="off" type="text" id="emp_unitFromDate" name="emp_unitFromDate[]" class="form-control datetimepicker" placeholder="yyyy-mm-dd">
                    </div>
                      </div>
              
                     
                      <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="emp_unitToDate">To <small>(Leave blank if still working)</small></label>
                        <input autocomplete="off" type="text" id="emp_unitToDate" name="emp_unitToDate[]" class="form-control datetimepicker" placeholder="yyyy-mm-dd">
                    </div>
                      </div>
                      
                    </div>
                  
                    </div>
                     <div class="row">
                    <div class="col-md-12">
                        <a  id="more_emp_rec" class="btn btn-sm btn-primary text-white">Add More</a>
                    </div>
                    </div>     


                        <hr class="my-4 white" />
                <!-- Address -->
                <h6 class="heading-small text-secondary mb-4">Civil Qualification</h6>
                <div class="civQual">

                    <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="c_course_name">Course Name</label>
                        <input id="c_course_name" name="c_course_name[]" class="form-control" placeholder="BSc"  autocomplete="off" type="text">
                        <div id="c_course_name_inputs"></div>
                      </div>
                    </div>
                  {{-- </div> --}}
                  {{-- <div class="row"> --}}
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="c_division">Division</label>
                        <input autocomplete="off" type="text" id="c_division" name="c_division[]" class="form-control" placeholder="A" >
                        <div id="c_division_inputs"></div>
                      </div>
                    </div>
                    </div>
                    </div>
                     <div class="row">
                    <div class="col-md-12">
                        <a  id="more_civ_qual" class="btn btn-sm btn-primary text-white">Add More</a>
                    </div>
                    </div>

                 <hr class="my-4 white" />
                <!-- Address -->
                <h6 class="heading-small text-secondary mb-4">Technical Qualification</h6>
                <div class="techQual">
                 <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="t_course_name">Course Name</label>
                        <input id="t_course_name" name="t_course_name[]" class="form-control" placeholder="BSc"  autocomplete="off" type="text">
                        {{-- <div id="t_course_name_inputs"></div> --}}
                      </div>
                    </div>
                     <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="t_division">Division</label>
                        <input autocomplete="off" type="text" id="t_division" name="t_division[]" class="form-control" placeholder="A" >
                        {{-- <div id="t_institue_inputs"></div> --}}
                      </div>
                    </div>
                 
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="t_institue">Institue</label>
                        <input autocomplete="off" type="text" id="t_institue" name="t_institue[]" class="form-control" placeholder="Alpha Bravo Institutes" >
                        {{-- <div id="t_institue_inputs"></div> --}}
                      </div>
                    </div>
                   
                      <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="t_fromDate">From</label>
                        <input autocomplete="off" type="text" id="t_fromDate" name="t_fromDate[]" class="form-control datetimepicker" placeholder="yyyy-mm-dd">
                    </div>
                      </div>
                      <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="t_toDate">To <small>(Leave blank if still in progress)</small></label>
                        <input autocomplete="off" type="text" id="t_toDate" name="t_toDate[]" class="form-control datetimepicker" placeholder="yyyy-mm-dd">
                      </div>
                      
                    </div>
                    </div>
                    </div>
                     <div class="row">
                    <div class="col-md-12">
                        <a  id="more_tech_qual" class="btn btn-sm btn-primary text-white">Add More</a>
                    </div>
                    </div>
                        <hr class="my-4 white" />
                <!-- Address -->
                <h6 class="heading-small text-secondary mb-4">Punishment Record</h6>
                <div class="punishRec">
                  <div class="row">
                     <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="punishment_name">Punishment Awarded</label>
                        <input autocomplete="off" autocomplete="off" type="text" id="punishment_name" name="punishment_name[]" class="form-control" placeholder="Punishment " >
                        {{-- <div id="t_institue_inputs"></div> --}}
                      </div>
                    </div>
                    
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="nature_offence">Nature Of Offence</label>
                        <input autocomplete="off" id="nature_offence" name="nature_offence[]" class="form-control" placeholder="Offence"  autocomplete="off" type="text">
                        {{-- <div id="t_course_name_inputs"></div> --}}
                      </div>
                    </div>
                      <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="punished_by">Punished By</label>
                           <select autocomplete="off" class="form-control" id="punished_by" name="punished_by[]">
                            <option value="" selected hidden>Please Select</option>
                            <option value="A">A</option>
                            <option value="B">B</option>
                            <option value="C">C</option>
                            <option value="D">D</option>
                           </select>
                        {{-- <input id="utilization_of_posting" name="utilization_of_posting" class="form-control datetimepicker" placeholder="yyyy-mm-dd" autocomplete="off" type="text"> --}}
                      </div>
                    </div>
                  </div>
               
                  
                    </div>
                     <div class="row">
                    <div class="col-md-12">
                        <a  id="more_punish_rec" class="btn btn-sm btn-primary text-white">Add More</a>
                        {{-- <a  id="more_punish_rec" class="btn btn-sm btn-primary text-white" onclick="$('.punishRec .row:last-child').remove()">delete</a> --}}
                    </div>
                    </div>
                            <div class="row">
                    <div class="col-md-12">
                      <input type="submit" value="Save" class="float-right btn btn-sm btn-success">
                    </div>
                 
                    </div>
                    </div>
                </div>
  
              </form>
            </div>
          </div>
        </div>
      </div>
      </div>
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery.datetimepicker.full.js') }}"></script>
             
<script>
$(document).ready(function() {


 $("#employeeForm").validate({
        rules: {
            employee_id: {
                minlength: 3,
                required: !0,
                remote: {
                  url: "{{ route('validate_empID')}}",
                  type: "post",
                  dataType: 'json',
                  data: {
                    employee_id: function() {
                      return $( "#employee_id" ).val();
                    }

              }
            }
            },
              rank: {
                minlength: 2,
                required: !0
            },
              bps: {
                minlength: 2,
                required: !0
            },  
            name: {
                minlength: 2,
                required: !0
            },
            cnic: {
                required: !0,
            },         
            domicile: {
                minlength: 2,
                required: !0
            },
            marital_status: {
                required: !0
            },
            present_address: {
                minlength: 2,
                required: !0
            },
            permanent_address: {
                minlength: 2,
                required: !0
            }
        },
        messages: {
            employee_id: {
                minlength: 'Please enter at least 3 characters',
                required: 'Please enter valid employee ID',
                remote: 'Employee ID already found!'
             
            },
              rank: {
                required: 'Please enter employee rank'
            },
              bps: {
                required: 'Please enter employee BPS'
            },  
            name: {
                minlength: 'Please enter at least 3 characters',
                required: 'Please enter employee full name'
            },
            cnic: {
                required: 'Please enter valid CNIC #'
            },         
            domicile: {
                minlength: 'Please enter at least 3 characters',
                required: 'Please enter domicile'
            },
            marital_status: {
                required: 'Please select marital status'
            },
            present_address: {
                minlength: 2,
                required: 'Please enter present address'
            },
            permanent_address: {
                minlength: 2,
                required: 'Please enter permanent address'
            }
        }
        
        
    });



});

$(document).ready(function() {
dtp();

  });  

    function dtp() {
       var today = new Date('Y-m-d');
    $('.datetimepicker').datetimepicker({
    timepicker:false,
    format:'Y-m-d'
    // maxDateTime: today

  });  
    }


var inp =  document.getElementById('image');
inp.addEventListener('change', function(e){
    var file = this.files[0];
    var reader = new FileReader();
    reader.onload = function(){
        document.getElementById('card-img-top').src = this.result;
        };
    reader.readAsDataURL(file);
    },false);

$(".file-wrapper").click(function () {
    $("#image").trigger('click');
});

$('#more_tech_qual').click(function(event) {

  var html = $('.techQual .row:first').html();
  // console.log(html);
 // $('.techQual').append('<hr>');
 $('.techQual').append(`<div class="row pt-5">${html}<div class="col-md-12"><a class="btn float-right btn-sm btn-danger" onclick="$(this).parent('.col-md-12').parent('.row').remove()">Remove</a></div></div>`);
dtp();
});

$('#more_punish_rec').click(function(event) {
  var html = $('.punishRec .row:first').html();
 // $('.punishRec').append('<hr>');
 $('.punishRec').append(`<div class="row pt-5">${html}<div class="col-md-12"><a class="btn float-right btn-sm btn-danger" onclick="$(this).parent('.col-md-12').parent('.row').remove()">Remove</a></div></div>`);
});


$('#more_emp_rec').click(function(event) {
  var html = $('.empRec .row:first').html();
 // $('.punishRec').append('<hr>');
 $('.empRec').append(`<div class="row pt-5">${html}<div class="col-md-12"><a class="btn float-right btn-sm btn-danger" onclick="$(this).parent('.col-md-12').parent('.row').remove()">Remove</a></div></div>`);
 dtp();
});



$('#more_civ_qual').click(function(event) {
  var html = $('.civQual .row:first').html();
 // $('.civQual').append('<hr>');
 $('.civQual').append(`<div class="row pt-5">${html}<div class="col-md-12"><a class="btn float-right btn-sm btn-danger" onclick="$(this).parent('.col-md-12').parent('.row').remove()">Remove</a></div></div>`);

});



// $('#button').click(function(event) {
//   /* Act on the event */
                        
// // var input1 ='<input id="c_course_name" name="c_course_name[]" class="form-control" placeholder="BSc"  autocomplete="off" type="text">';
// // var input2 ='<input autocomplete="off" type="text" id="c_division" name="c_division[]" class="form-control" placeholder="A" >';
// var parent1 = document.getElementById("c_course_name_inputs");
// // parent1.append(input1);

// var mi = document.createElement("input");
// mi.setAttribute('type', 'text');
// mi.setAttribute('class', 'mt-2 form-control');
// mi.setAttribute('id', 'c_course_name');
// mi.setAttribute('name', 'c_course_name[]');
// mi.setAttribute('placeholder', 'BSc');
// parent1.appendChild(mi);


// var parent2 = document.getElementById("c_division_inputs");

// var mo = document.createElement("input");
// mo.setAttribute('type', 'text');
// mo.setAttribute('class', 'mt-2 form-control');
// mo.setAttribute('id', 'c_division');
// mo.setAttribute('name', 'c_division[]');
// mo.setAttribute('placeholder', 'A');
// parent2.appendChild(mo);


// // parent2.append(input2);
// });
</script>
@endsection