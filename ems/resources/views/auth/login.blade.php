@extends('layouts.app')
@section('content')

<style>
.login-form{
    padding-top: 45px;
}
    .card-body .avatar{
    position: absolute;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 5px;
    width: 95px;
    height: 95px;
    border-radius: 50%;
    z-index: 9;
    background: rgba(115,113,81,1);
    padding: 15px;
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
}
</style>
<div class="container ">
    <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
            <div class="car bg-form shadow rounded">
          
                  
                <div class="card-body px-lg-5 mt-5">

                    <div class="avatar">
                    <img src="/images/avatar.png" alt="Avatar">
                </div>
                 
                    {{-- <div class="text-center text-muted mb-4">
                        <h1 class="text-white">SIGN IN</h1>
                        <hr>
                    </div> --}}
                    <div class="login-form">
                    <form role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="{{ $errors->has('email') ? ' has-error' : '' || $errors->has('password') ? ' has-error' : '' }}">
                            @if ($errors->has('email') || $errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group mb-3 {{-- {{ $errors->has('email') ? ' has-error' : '' }} --}}">
                            <label for="email" class="col-md-12 control-label text-white">E-Mail Address</label>
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                </div>
                                <input class="form-control" placeholder="Email" type="email" name="email" value="{{ old('email') }}" required autofocus>
                                {{-- @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif --}}
                            </div>
                        </div>
                        <div class="form-group {{-- {{ $errors->has('password') ? ' has-error' : '' }} --}}">
                            <label for="password" class="col-md-12 control-label text-white">Password</label>
                            <div class="input-group input-group-alternative">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                </div>
                                <input class="form-control" placeholder="Password" type="password" name="password" required>
                                {{--   @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif --}}
                            </div>
                        </div>
                        <div class="custom-control custom-control-alternative custom-checkbox">
                            <input class="custom-control-input" id=" customCheckLogin" name="remember" type="checkbox"{{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label text-white" for=" customCheckLogin">
                                <span class="text-muted">Remember me</span>
                            </label>
                        </div>

                        <div class="text-center">
                            <button type="submit" class="btn btn-primary my-4">Sign in</button>
                        </div>
                    </form>
                      <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                        @endforeach
                        
                    </div>
                </div>
                </div>
            </div>
            {{-- <div class="row mt-3">
                <div class="col-6">
                    <a href="{{ route('password.request') }}" class="text-light"><small>Forgot password?</small></a>
                </div>
               
            </div> --}}
        </div>
    </div>
</div>
@endsection
