<!-- Sidenav -->
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
  <div class="scrollbar-inner">
    <!-- Brand -->
    <div class="sidenav-header  align-items-center">
      <a class="navbar-brand" href="javascript:void(0)">
        <img src="{{asset('/images/waqarlogo.png')}}" class="navbar-brand-img" alt="...">
      </a>
    </div>
    <div class="navbar-inner">
      <!-- Collapse -->
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <!-- Nav items -->
        <ul class="navbar-nav">
         {{--  <li class="nav-item">
            <a class="nav-link active" href="examples/dashboard.html">
              <i class=" text-primary"></i>
              <span class="nav-link-text">Dashboard</span>
            </a>
          </li> --}}
             <li class="nav-item">
          <a class="nav-link {{\Request::is('search') ? 'active' : ''}}" href="{{ route('employee.search')}}">
          <i class=""><svg width="20px" height="20px" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="file-search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" class="svg-inline--fa fa-file-search fa-w-20 fa-2x"><path fill="currentColor" d="M605.65 487.05l-88.89-88.88c16.86-21.68 27.31-48.58 27.31-78.17a128 128 0 1 0-128 128c29.55 0 56.43-10.42 78.1-27.24l88.9 88.89a8 8 0 0 0 11.32 0l11.31-11.31a8 8 0 0 0-.05-11.29zM416.05 416a96 96 0 1 1 96-96 96.12 96.12 0 0 1-96 96zM368 480H80a16 16 0 0 1-16-16V48.09a16 16 0 0 1 16-16h176v104a23.93 23.93 0 0 0 24 24l136-.09v-28a48.23 48.23 0 0 0-14.1-34L318 14.1A48 48 0 0 0 284.08 0H80a48.16 48.16 0 0 0-48 48.09V464a48 48 0 0 0 48 48h288a47.87 47.87 0 0 0 45.15-32.29 159.9 159.9 0 0 1-33.92-4.36A15.91 15.91 0 0 1 368 480zM288 32.59a15.73 15.73 0 0 1 7.4 4.2l83.9 83.89a15.75 15.75 0 0 1 4.2 7.39H288z" class=""></path></svg></i>
          <span class="nav-link-text">SEARCH</span>
        </a>
      </li>
          <li class="nav-item">
            <a class="nav-link {{\Request::is('create_employee') ? 'active' : ''}}" href="{{ route('employee.index')}}">
            <i class=""><svg width="20px" height="20px" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="plus-circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-plus-circle fa-w-16 fa-2x"><path fill="currentColor" d="M384 250v12c0 6.6-5.4 12-12 12h-98v98c0 6.6-5.4 12-12 12h-12c-6.6 0-12-5.4-12-12v-98h-98c-6.6 0-12-5.4-12-12v-12c0-6.6 5.4-12 12-12h98v-98c0-6.6 5.4-12 12-12h12c6.6 0 12 5.4 12 12v98h98c6.6 0 12 5.4 12 12zm120 6c0 137-111 248-248 248S8 393 8 256 119 8 256 8s248 111 248 248zm-32 0c0-119.9-97.3-216-216-216-119.9 0-216 97.3-216 216 0 119.9 97.3 216 216 216 119.9 0 216-97.3 216-216z" class=""></path></svg></i>
            <span class="nav-link-text">Create</span>
          </a>
        </li>
  
 
    
            <li class="nav-item">
            <a class="nav-link" href="#nav-menu" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="nav-menu">
              <i class="ni ni-settings-gear-65 text-dark"></i>
              <span class="nav-link-text">Settings</span>
            </a>
            <div class="collapse" id="nav-menu">
              <ul class="nav nav-sm flex-column">
                  <li class="nav-item">
        <a class="nav-link {{\Request::is('units') ? 'active' : ''}}" href="{{ route('units.index')}}">
        <i class=""><svg width="20px" height="20px" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="building" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-building fa-w-14 fa-2x"><path fill="currentColor" d="M192 107v40c0 6.627-5.373 12-12 12h-40c-6.627 0-12-5.373-12-12v-40c0-6.627 5.373-12 12-12h40c6.627 0 12 5.373 12 12zm116-12h-40c-6.627 0-12 5.373-12 12v40c0 6.627 5.373 12 12 12h40c6.627 0 12-5.373 12-12v-40c0-6.627-5.373-12-12-12zm-128 96h-40c-6.627 0-12 5.373-12 12v40c0 6.627 5.373 12 12 12h40c6.627 0 12-5.373 12-12v-40c0-6.627-5.373-12-12-12zm128 0h-40c-6.627 0-12 5.373-12 12v40c0 6.627 5.373 12 12 12h40c6.627 0 12-5.373 12-12v-40c0-6.627-5.373-12-12-12zm-128 96h-40c-6.627 0-12 5.373-12 12v40c0 6.627 5.373 12 12 12h40c6.627 0 12-5.373 12-12v-40c0-6.627-5.373-12-12-12zm128 0h-40c-6.627 0-12 5.373-12 12v40c0 6.627 5.373 12 12 12h40c6.627 0 12-5.373 12-12v-40c0-6.627-5.373-12-12-12zm140 205v20H0v-20c0-6.627 5.373-12 12-12h20V24C32 10.745 42.745 0 56 0h336c13.255 0 24 10.745 24 24v456h20c6.627 0 12 5.373 12 12zm-64-12V32H64v448h128v-85c0-6.627 5.373-12 12-12h40c6.627 0 12 5.373 12 12v85h128z" class=""></path></svg></i>
        <span class="nav-link-text">UNITS</span>
      </a>
    </li>
     <li class="nav-item">
                  <a class="nav-link" href="#footer-dropdown" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="footer-dropdown">
                    <i class=" text-dark"><svg width="20px" height="20px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="users" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" class="svg-inline--fa fa-users fa-w-20 fa-2x"><path fill="currentColor" d="M96 224c35.3 0 64-28.7 64-64s-28.7-64-64-64-64 28.7-64 64 28.7 64 64 64zm448 0c35.3 0 64-28.7 64-64s-28.7-64-64-64-64 28.7-64 64 28.7 64 64 64zm32 32h-64c-17.6 0-33.5 7.1-45.1 18.6 40.3 22.1 68.9 62 75.1 109.4h66c17.7 0 32-14.3 32-32v-32c0-35.3-28.7-64-64-64zm-256 0c61.9 0 112-50.1 112-112S381.9 32 320 32 208 82.1 208 144s50.1 112 112 112zm76.8 32h-8.3c-20.8 10-43.9 16-68.5 16s-47.6-6-68.5-16h-8.3C179.6 288 128 339.6 128 403.2V432c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48v-28.8c0-63.6-51.6-115.2-115.2-115.2zm-223.7-13.4C161.5 263.1 145.6 256 128 256H64c-35.3 0-64 28.7-64 64v32c0 17.7 14.3 32 32 32h65.9c6.3-47.4 34.9-87.3 75.2-109.4z" class=""></path></svg></i>
                    <span class="nav-link-text">USERS</span>
                  </a>
                  <div class="collapse" id="footer-dropdown">
                    <ul class="nav nav-sm flex-column">
                      <li class="nav-item">
                        <a href="" class="nav-link">Manage</a>
                      </li>
                      <li class="nav-item">
                        <a href="{{ route('users.register')}}" class="nav-link">Register</a>
                      </li>
                      
                      <!--  <li class="nav-item">
                        <a href="#navbar-multilevel" class="nav-link" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-multilevel">Multi level</a>
                        <div class="collapse show" id="navbar-multilevel" style="">
                          <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">Third level menu</a>
                            </li>
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">Just another link</a>
                            </li>
                            <li class="nav-item">
                              <a href="#!" class="nav-link ">One last link</a>
                            </li>
                          </ul>
                        </div>
                      </li> -->
                    </ul>
                  </div>
                </li>
    <li class="nav-item">
        <a class="nav-link {{\Request::is('units') ? 'active' : ''}}" href="{{ route('units.index')}}">
        <i class=""><svg width="20px" height="20px" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="shield-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-shield-alt fa-w-16 fa-2x"><path fill="currentColor" d="M466.5 83.7l-192-80a48.15 48.15 0 0 0-36.9 0l-192 80C27.7 91.1 16 108.6 16 128c0 198.5 114.5 335.7 221.5 380.3 11.8 4.9 25.1 4.9 36.9 0C360.1 472.6 496 349.3 496 128c0-19.4-11.7-36.9-29.5-44.3zM256.1 446.3l-.1-381 175.9 73.3c-3.3 151.4-82.1 261.1-175.8 307.7z" class=""></path></svg></i>
        <span class="nav-link-text">ACCESS</span>
      </a>
    </li>
   
  </ul>
  <!-- Divider -->
  <hr class="my-3">
  <!-- Heading -->
  <h6 class="navbar-heading p-0 text-muted">
  {{-- <span class="docs-normal">Documentation</span> --}}
  </h6>
  
</div>

</div>
</div>

</nav>
<!-- Main content -->
<div class="main-content" id="panel">
<!-- Topnav -->
<nav class="navbar navbar-top navbar-expand navbar-dark bg-navbar border-bottom">
<div class="container-fluid">
<div class="collapse navbar-collapse" id="navbarSupportedContent">
  <!-- Search form -->
 {{--  <form class="navbar-search navbar-search-light form-inline mr-sm-3" id="navbar-search-main">
    <div class="form-group mb-0">
      <div class="input-group input-group-alternative input-group-merge">
        <div class="input-group-prepend">
          <span class="input-group-text"><i class="fas fa-search"></i></span>
        </div>
        <input class="form-control" placeholder="Search" type="text">
      </div>
    </div>
    <button type="button" class="close" data-action="search-close" data-target="#navbar-search-main" aria-label="Close">
    <span aria-hidden="true">×</span>
    </button>
  </form> --}}
  
  <ul class="navbar-nav align-items-center  ml-md-auto ">
    <li class="nav-item dropdown">
      <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <div class="media align-items-center">
          <span class="avatar avatar-sm rounded-circle">
            <img alt="-" src="{{ asset('/images/pakarmy_logo.png')}}">
          </span>
          <div class="media-body  ml-2  d-lg-block">
            <span class="mb-0 text-sm  font-weight-bold">{{ Auth::user()->name}}</span>
          </div>
        </div>
      </a>
      <div class="dropdown-menu  dropdown-menu-right ">
        
        <a class="dropdown-item" href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          <span>Logout</span>
          <span class="float-right">
          <svg width="15px" height="15px" aria-hidden="true" focusable="false" data-prefix="fal" data-icon="sign-out" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-sign-out fa-w-16 fa-2x"><path fill="currentColor" d="M48 64h132c6.6 0 12 5.4 12 12v8c0 6.6-5.4 12-12 12H48c-8.8 0-16 7.2-16 16v288c0 8.8 7.2 16 16 16h132c6.6 0 12 5.4 12 12v8c0 6.6-5.4 12-12 12H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48zm279 19.5l-7.1 7.1c-4.7 4.7-4.7 12.3 0 17l132 131.4H172c-6.6 0-12 5.4-12 12v10c0 6.6 5.4 12 12 12h279.9L320 404.4c-4.7 4.7-4.7 12.3 0 17l7.1 7.1c4.7 4.7 12.3 4.7 17 0l164.5-164c4.7-4.7 4.7-12.3 0-17L344 83.5c-4.7-4.7-12.3-4.7-17 0z" class=""></path></svg>
        </span>
      </a>
      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
      </form>
      
    </div>
  </li>
</ul>
</div>
</div>
</nav>
<!-- Header -->