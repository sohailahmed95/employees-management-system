
    <!-- Scripts -->
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
  <script src="./vendor/jquery/dist/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
  <script src="./vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="./vendor/js-cookie/js.cookie.js"></script>
  <script src="./vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
  <script src="./vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
  <!-- Optional JS -->
  <script src="./vendor/chart.js/dist/Chart.min.js"></script>
  <script src="./vendor/chart.js/dist/Chart.extension.js"></script>
  <!-- Argon JS -->
  <script src="./js/argon.js?v=1.2.0"></script>
  <script src="./js/jquery.datetimepicker.full.js"></script>
  {{-- <script src="./js/range_dates.js"></script> --}}
  {{-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script> --}}
  
</body>
</html>

