@extends('layouts.app')
@section('content')

<div class="container">
   <div class="row">
        <div class="col-xl-12 order-xl-1">
          <div class="card bg-form">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Add New Unit</h3>
                </div>
                <div class="col-4 text-right">
                  <a href="{{url()->previous()}}" class="btn btn-sm btn-primary">go back</a>
                </div>
              </div>
            </div>
            <div class="card-body">
                {{-- <h6 class="heading-small text-muted mb-4">Add New Unit</h6> --}}
                <div class="pl-lg-4">
                           <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}"><strong>{{ Session::get('alert-' . $msg) }}</strong> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                        @endforeach
                     
                      @if ($errors->has('unit'))
                      <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <span class="alert-text"><strong>{{ $errors->first('unit') }}</strong></span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      @endif
                    </div>
                  <div class="row">
        <div class="col-lg-8">

                 <div class="table-responsive">
                  <h3 class="text-white">Units Record</h3>
              <table class="table align-items-center text-center table-flush table-bordered">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col" class="sort" data-sort="name">Id</th>
                    <th scope="col" class="sort" data-sort="budget">Name</th>
                    <th scope="col" class="sort" data-sort="completion">Action</th>
                  </tr>
                </thead>
                <tbody class="list">
              @foreach($units as $unit)
                  <tr>
                    <th scope="row">
                      <div class="media align-items-center">
                       
                        <div class="media-body">
                          <span class="name mb-0 text-sm">{{$unit->id}}</span>
                        </div>
                      </div>
                    </th>
                    
                    <td>
                      <div class=" align-items-center">
                       {{$unit->name}}
                      </div>
                    </td>
                    <td class="">
                     {{--    <a class="btn btn-sm btn-icon-only text-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">dfsgdfg
                          <i class="fas fa-ellipsis-v"></i>
                        </a> --}}
                        {{-- <button class="btn btn-danger btn-sm">Delete</button> --}}
                     <form action="{{url('delete_unit', [$unit->id])}}" method="get">
   {{method_field('DELETE')}}
                  {{ csrf_field() }}
   <input type="submit" class="w-100 btn btn-sm btn-danger" value="Delete"/>
</form>
                    </td>
                  </tr>
          @endforeach
          
         
                </tbody>
              </table>
            </div>
                </div>
                    <div class="col-lg-4">
              <form id="createunitForm" action="{{route('units.create')}}">
                  {{ csrf_field() }}
                     

                      <div class="form-group">
                        <label class="form-control-label text-white" for="unit">Enter Unit Name</label>
                        <input type="text" id="unit" name="unit" class="form-control" placeholder="IAS&C" >
                  <input type="submit" value="Add" class="w-100 mt-4 d-block btn btn-md btn-success">
                      </div>                 
              </form>
              {{-- @php(dd($units)); --}}
                      </div>
            
            
            </div>
            </div>
          </div>
        </div>
      </div>
@endsection