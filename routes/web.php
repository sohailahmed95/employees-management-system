<?php
use Illuminate\Http\Request;
use App\Http\Middleware\CheckIpMiddleware;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['checkIp'])->group(function () {
  

Route::get('/', function () {
    return view('home');
});


Route::get('download_zip', function (Request $request) {
		return Response::download($request->url);
})->name('download_zip');


Route::get('download_merged', function (Request $request) {
		return Response::download($request->url);
})->name('download_merged');



// Authentication Routes...
// 
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
// 
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
$this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
// 
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');



Route::get('/home', 'HomeController@index')->name('home');



// Route::namespace('Admin')->group(function () {
Route::get('/register_user', 'Admin\Users@register')->name('users.register');
// Route::get('/users', 'Admin\Users@users_list')->name('users');

// Route::post('/user_activate', 'Admin\Users@user_activate')->name('user_activate');
// Route::post('/user_deactivate', 'Admin\Users@user_deactivate')->name('user_deactivate');

// ************ Employee Controller ******************************

Route::get('/create_employee', 'Admin\EmployeeController@index')->name('employee.index');
Route::post('/create_new', 'Admin\EmployeeController@create')->name('employee.create');
Route::get('/search', 'Admin\EmployeeController@show')->name('employee.search');
Route::get('/view', 'Admin\EmployeeController@view')->name('employee.view');
Route::post('/validate_empID', 'Admin\EmployeeController@validate_empID')->name('validate_empID');

// ************** End Employee Controller ****************************



// ************ EDIT Employement Controller ******************************

Route::get('/update_employement', 'Admin\EmployementController@index')->name('employement.edit');
Route::post('/update_employement_info', 'Admin\EmployementController@update')->name('employement.update');
Route::get('/remove_employement_info', 'Admin\EmployementController@destroy')->name('employement.remove');


// ************** End EDIT Employement Controller ****************************
// 
// 
// ************ Add Employement Controller ******************************

Route::get('/form_employement_record', 'Admin\EmployementController@create')->name('empRec.add');
Route::post('/add_employement_info', 'Admin\EmployementController@store')->name('empRec.store');


// ************** End Add Employement Controller ****************************




// ************ EDIT Employee Controller ******************************

Route::get('/employee_info', 'Admin\EmployeeController@edit')->name('employee.edit');
Route::post('/update_employee_info', 'Admin\EmployeeController@update')->name('employee.update');

// ************** End EDIT Employee Controller ****************************



// ************ EDIT Tech Qualification Controller ******************************

Route::get('/edit_tech_qualification', 'Admin\TechQualController@index')->name('techqual.edit');
Route::post('/edit_tech_qualification_update', 'Admin\TechQualController@update')->name('techqual.update');
Route::get('/remove_tech_qualification', 'Admin\TechQualController@destroy')->name('techqual.remove');

// ************** End EDIT Tech Qualification Controller ****************************
// 
// 
// 
// // ************ Add Tech Qualification Controller ******************************

Route::get('/form_tech_qualification', 'Admin\TechQualController@create')->name('techQual.add');
Route::post('/add_tech_qualification', 'Admin\TechQualController@store')->name('techqual.save');

// ************** End Add Tech Qualification Controller ****************************



// ************ EDIT Civil Qualification Controller ******************************

Route::get('/edit_civil_qualification', 'Admin\CivQualController@index')->name('civqual.edit');
Route::post('/edit_civil_qualification_update', 'Admin\CivQualController@update')->name('civqual.update');
Route::get('/remove_civil_qualification', 'Admin\CivQualController@destroy')->name('civqual.remove');

// ************** End EDIT Civil Qualification Controller ****************************// 


// ************ Add Civil Qualification Controller ******************************

Route::get('/form_civil_qualification', 'Admin\CivQualController@create')->name('civQual.add');
Route::post('/add_civil_qualification', 'Admin\CivQualController@store')->name('civQual.save');

// ************** End Add Civil Qualification Controller ****************************


// ************ EDIT Punishment Record Controller ******************************

Route::get('/edit_punish_rec', 'Admin\PunishRecController@index')->name('punishrec.edit');
Route::post('/edit_punish_rec_update', 'Admin\PunishRecController@update')->name('punishrec.update');
Route::get('/remove_punish_rec', 'Admin\PunishRecController@destroy')->name('punishrec.remove');

// ************** End EDIT Punishment Record Controller ****************************

// ************ Add Punishment Record Controller ******************************

Route::get('/form_punish_rec', 'Admin\PunishRecController@create')->name('punishrec.add');
Route::post('/add_punish_rec', 'Admin\PunishRecController@store')->name('punishrec.save');

// ************** End Add Punishment Record Controller ****************************


// ************ UNIT Controller ******************************

Route::get('/units', 'Admin\UnitController@index')->name('units.index');
Route::get('/create_unit', 'Admin\UnitController@create')->name('units.create');
Route::get('/delete_unit/{id}', 'Admin\UnitController@destroy')->name('units.delete');

// ************** End UNIT Controller ****************************




// ************ PDF Controller ******************************

Route::get('employee/downloadpdf', 'Admin\PDFController@printPDF')->name('downloadpdf');
Route::get('employee/downloadpdf_all', 'Admin\PDFController@download_pdf_all')->name('download_pdf_all');
Route::get('employee/downloadpdf_merged', 'Admin\PDFController@pdf_merged')->name('pdf_merged');

// ************** End PDF Controller ****************************


Route::post('search_filter', 'Admin\EmployeeController@search_filter')->name('fetch_by_date');


// });
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');






});