@include('includes.header')
 {{-- @php(dd(Auth::user()->is_admin)) --}}
@if(Auth::check() && Auth::user())
@include('includes.navigation')
@endif
@yield('content')

@include('includes.footer')