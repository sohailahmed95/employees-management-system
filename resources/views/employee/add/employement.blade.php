@extends('layouts.app')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.datetimepicker.css') }}">

<div class="container">
   <div class="row">
        <div class="col-xl-12 order-xl-1">
          <div class="card bg-form">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Add Employement Record</h3>
                </div>
                <div class="col-4 text-right">
                  <a href="{{url()->previous()}}" title="Go To Previous Page" class="btn btn-sm btn-primary">go back</a>
                </div>
              </div>
            </div>
            {{-- @php(dd($data)) --}}
            <div class="card-body">
                  <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}"><strong>{{ Session::get('alert-' . $msg) }}</strong> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                        @endforeach
                      </div>
              <form id="employeeForm" action="{{route('empRec.store')}}" method="post" >
                  {{ csrf_field() }}

                <!-- Address -->
                {{-- <h6 class="heading-small text-secondary mb-4">Employement Record</h6> --}}
                    @php($i =1)

                  {{-- <h3 class="text-white" style="  text-decoration: underline;">Record # {{ $i++}}</h3> --}}

                  <input type="text" hidden name="employee_id" id="employee_id" value="{{$data[0]->employee_id}}">
                  <div id="add_emp_rec_box">
                  <div class="row">
                      <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="emp_unit_id">Unit</label>
                            <select class="form-control" id="emp_unit_id" required name="emp_unit_id[]">
                             @foreach($units as $unit)
                              <option  value=" {{  old('emp_unit_id',$unit->id) }}">{{$unit->name}}</option>
                             @endforeach                        

                           </select>
                        {{-- <input type="text" id="present_unit_id" name="present_unit_id" class="form-control" placeholder="ASID"> --}}
                      </div>
                    </div>
                     <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="emp_unitFromDate">From</label>
                        <input autocomplete="off" type="text" id="emp_unitFromDate" value="" required name="emp_unitFromDate[]" class="form-control datetimepicker" placeholder="N/A">
                    </div>
                      </div>
              
                     
                      <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="emp_unitToDate">To <small>(Leave blank if still working)</small></label>
                        <input autocomplete="off" type="text" id="emp_unitToDate" value="" required name="emp_unitToDate[]" class="form-control datetimepicker" placeholder="N/A">
                    </div>
                      </div>
                      
                    </div>
                    </div>
                  
                        <div class="row">
                    <div class="col-md-12">
                      <input type="submit" value="Save" class="float-right btn btn-sm btn-success ">
                      <input type="button" value="Add More" id="add_more" class="float-right btn btn-sm btn-success mr-2">
                    </div>
                 
                    </div>
                 {{--               <div class="row">
                    <div class="col-md-12">
                      <input type="submit" value="Update" class="float-right btn btn-sm btn-success">
                    </div>
                 
                    </div>
               --}}
              
              </form>
            </div>
          </div>
        </div>
      </div>
      </div>
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery.datetimepicker.full.js') }}"></script>
             
<script>
$(document).ready(function() {
dtp();

  });  

    function dtp() {
       var today = new Date('Y-m-d');
    $('.datetimepicker').datetimepicker({
    timepicker:false,
    format:'Y-m-d'
    // maxDateTime: today

  });  
    }

// });

$('#add_more').click(function(event) {

  var html = $('#add_emp_rec_box .row:first').html();
  // console.log(html);
 // $('.techQual').append('<hr>');
 $('#add_emp_rec_box').append(`<div class="row pt-5">${html}<div class="col-md-12"><a class="btn bttt btn-sm btn-danger" onclick="$(this).parent('.col-md-12').parent('.row').remove()">Remove</a></div></div>`);
dtp();
});

</script>
@endsection