@extends('layouts.app')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.datetimepicker.css') }}">

<div class="container">
   <div class="row">
        <div class="col-xl-12 order-xl-1">
          <div class="card bg-form">
            <div class="card-header">
              <div class="row align-items-center">
                <div class="col-8">
                  <h3 class="mb-0">Update Technical Qualification</h3>
                </div>
                <div class="col-4 text-right">
                  <a href="{{url()->previous()}}" title="Go To Previous Page" class="btn btn-sm btn-primary">go back</a>
                  <a href="{{ route('techQual.add', ['employee_id' => $data[0]->employee_id]) }}" title="Add More Records" class="btn btn-sm btn-success"><i class="fa fa-plus-circle"></i></a>

                </div>
              </div>
            </div>
            {{-- @php(dd($data)) --}}
            <div class="card-body">
                  <div class="flash-message">
                        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}"><strong>{{ Session::get('alert-' . $msg) }}</strong> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                        @endforeach
                      </div>
              <form id="employeeForm" action="{{route('techqual.update')}}" method="post" >
                  {{ csrf_field() }}

                <!-- Address -->
                {{-- <h6 class="heading-small text-secondary mb-4"></h6> --}}
                @php($i=1)
                  @foreach($data as $item)
                   <div class="row">
                   <div class="col-md-6">
                      <h3 class="text-white" style="  text-decoration: underline;">Record # {{ $i++}}</h3>

                   </div>
                   <div class="col-md-6">
                     <a class="btn btn-danger btn-sm float-right" onclick="remove_record('{{$item->from}}','{{$item->to}}')"><i class="fa fa-trash"></i></a>
                   </div>
                 </div>
                  <input type="text" hidden name="employee_id" id="employee_id" value="{{$item->employee_id}}">

                 <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="t_course_name">Course Name</label>
                        <input id="t_course_name" name="t_course_name[]" value="{{$item->course}}" class="form-control" placeholder="N/A"  type="text">
                        {{-- <div id="t_course_name_inputs"></div> --}}
                      </div>
                    </div>
                     <div class="col-lg-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="t_division">Division</label>
                        <input type="text" id="t_division" name="t_division[]" value="{{$item->division}}" class="form-control" placeholder="N/A" >
                        {{-- <div id="t_institue_inputs"></div> --}}
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="t_institue">Institue</label>
                        <input type="text" id="t_institue" name="t_institue[]" value="{{$item->institute}}" class="form-control" placeholder="N/A" >
                        {{-- <div id="t_institue_inputs"></div> --}}
                      </div>
                    </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                      <div class="form-group">
                        @php($fromDate = \Carbon\Carbon::parse($item->from)->format('Y-m-d'))
                        @php($toDate = \Carbon\Carbon::parse($item->to)->format('Y-m-d'))
                        <label class="form-control-label text-white" for="t_fromDate">From</label>
                        <input type="text" id="t_fromDate" name="t_fromDate[]" class="form-control datetimepicker" placeholder="N/A" value="{{ isset($item->from) && !is_null($item->from) ? $fromDate : ''}}">
                    </div>
                      </div>
                      <div class="col-md-6">
                      <div class="form-group">
                        <label class="form-control-label text-white" for="t_toDate">To <small>(Leave blank if still in progress)</small></label>
                        <input type="text" id="t_toDate" name="t_toDate[]" class="form-control datetimepicker" placeholder="N/A" value="{{ isset($item->to) && !is_null($item->from) ? $toDate : ''}}">
                    </div>
                      </div>
                      
                    </div>
                    @endforeach

                        <div class="row">
                    <div class="col-md-12">
                      <input type="submit" value="Update" class="float-right btn btn-sm btn-success">
                    </div>
                 
                    </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    <script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery.datetimepicker.full.js') }}"></script>
             
<script>
$(document).ready(function() {
  var today = new Date('Y-m-d');
    $('.datetimepicker').datetimepicker({
    timepicker:false,
    format:'Y-m-d'
    // maxDateTime: today

  });  

});

function remove_record(from, to){
  // console.log(a, b);
var confirmation = confirm("Do you really want to remove this record ?");

if(confirmation){
    $.ajax({
    url:' {{ route('techqual.remove')}}',
    type: 'GET',
    data: {
      from, to
    },
  })
  .done(function() {
      // alert('Record Removed!');
       location.reload();
  })
  .fail(function() {
      alert('Something is wrong!');
  });
}
  
}

// $(document).on('click', '#deletee', function(event) {
//   event.preventDefault();
//   /* Act on the event */
//   alert($(this).html());
// });
</script>
@endsection